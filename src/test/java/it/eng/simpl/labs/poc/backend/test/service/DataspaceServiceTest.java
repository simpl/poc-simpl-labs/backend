package it.eng.simpl.labs.poc.backend.test.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;

import it.eng.simpl.labs.poc.entity.CategoryEntity;
import it.eng.simpl.labs.poc.entity.DataspaceEntity;
import it.eng.simpl.labs.poc.entity.NodeEntity;
import it.eng.simpl.labs.poc.enumeration.DataspaceStatusEnum;
import it.eng.simpl.labs.poc.mapper.DataspaceMapper;
import it.eng.simpl.labs.poc.model.Dataspace;
import it.eng.simpl.labs.poc.model.User;
import it.eng.simpl.labs.poc.repository.ComponentRepository;
import it.eng.simpl.labs.poc.repository.DataspaceRepository;
import it.eng.simpl.labs.poc.service.DataspaceService;
import it.eng.simpl.labs.poc.service.DeploymentService;

@ExtendWith(MockitoExtension.class)
class DataspaceServiceTest {
	
	@Mock
	private DataspaceRepository dataspaceRepository;
	
	@Mock
	private ComponentRepository componentRepository;
	
	@Mock
	private DataspaceMapper dataspaceMapper;
	
	@Mock
	private DeploymentService deploymentService;

	@InjectMocks
	private DataspaceService dataspaceService;
	
	private User user;
	private List<DataspaceEntity> dataspaceEntityList;
	private List<Dataspace> dataspaceModelList;
	private Dataspace dataspaceModel;
	private DataspaceEntity dataspaceEntity;
	private Stream<Dataspace> dataspaceStream;
	
	@BeforeEach
	public void setup(){
		user=User.builder().email("test@test.com").username("testUser").build();
		dataspaceEntity=new DataspaceEntity();
		dataspaceEntity.setId(1L);
		dataspaceEntity.setName("Test dataspace");
		List<CategoryEntity> categoryEntityList = new ArrayList<>();
		CategoryEntity categoryEntity = new CategoryEntity();
		categoryEntityList.add(categoryEntity);
		NodeEntity nodeEntity = new NodeEntity();
		nodeEntity.setCategoryList(categoryEntityList);
		List<NodeEntity> nodeEntityList = new ArrayList<>();
		nodeEntityList.add(nodeEntity);
		dataspaceEntity.setNodeList(nodeEntityList);
		dataspaceEntityList=new ArrayList<>();
		dataspaceEntityList.add(dataspaceEntity);
		dataspaceModel=Dataspace.builder().build();
		BeanUtils.copyProperties(dataspaceEntity, dataspaceModel);
		dataspaceModelList=new ArrayList<>();
		dataspaceModelList.add(dataspaceModel);
		dataspaceStream=dataspaceModelList.stream();
	}
	
	@DisplayName("Test for DataspaceService.findAll")
    @Test
    void givenUser_whenFind_thenReturnDataspaceList(){
		given(dataspaceRepository.findAllByUserId(user.getUsername()))
	    	.willReturn(dataspaceEntityList);
		
		given(dataspaceMapper.createFromEntities(anyList()))
			.willReturn(dataspaceStream);
		
		List<Dataspace> dataspaceList = dataspaceService.findAll(user);
		
        assertThat(dataspaceList).isNotNull()
        	.hasSizeGreaterThan(0);
	}
	
	@DisplayName("Test for DataspaceService.save")
    @Test
    void givenDataspace_whenSave_thenReturnDataspace(){
		given(dataspaceMapper.createFromDto(any(Dataspace.class)))
			.willReturn(dataspaceEntity);
		
		given(dataspaceMapper.createFromEntity(any(DataspaceEntity.class)))
			.willReturn(dataspaceModel);
		
		given(dataspaceRepository.saveAndFlush(any(DataspaceEntity.class)))
			.willReturn(dataspaceEntity);
		
		Dataspace dataspaceSaved = dataspaceService.save(dataspaceModel, user);
		
        assertThat(dataspaceSaved).isNotNull()
        	.hasFieldOrPropertyWithValue("status", DataspaceStatusEnum.REQUESTED);
	}
	
	@DisplayName("Test for DataspaceService.delete")
    @Test
    void givenDataspace_whenDelete_thenReturnDataspace(){
		dataspaceEntity.setStatus(DataspaceStatusEnum.RUNNING);
		given(dataspaceRepository.findByIdAndUserId(dataspaceModel.getId(),user.getUsername()))
			.willReturn(dataspaceEntity);
		
		Dataspace dataspaceModelToDelete=Dataspace.builder().build();
		BeanUtils.copyProperties(dataspaceModel, dataspaceModelToDelete);
		dataspaceModelToDelete.setStatus(DataspaceStatusEnum.REQUESTED_DELETION);
		given(dataspaceMapper.createFromEntity(any()))
			.willReturn(dataspaceModelToDelete);
		
		Dataspace dataspaceDeletionRequested = dataspaceService.delete(dataspaceModel.getId(), user);
		
        assertThat(dataspaceDeletionRequested).isNotNull()
        	.hasFieldOrPropertyWithValue("status", DataspaceStatusEnum.REQUESTED_DELETION);
	}
	
	@DisplayName("Test for DataspaceService.findAll")
    @Test
    void findAll(){
		
		given(dataspaceRepository.findAll()).willReturn(dataspaceEntityList);
		given(dataspaceMapper.createFromEntities(anyList())).willReturn(dataspaceStream);
		
		List<Dataspace> dataspaceList = dataspaceService.findAll();
        
		assertThat(dataspaceList).isNotNull().hasSizeGreaterThan(0);
        
	}
	
	@DisplayName("Test for DataspaceService.findById")
    @Test
    void findById(){
		
		given(dataspaceRepository.findById(anyLong())).willReturn(Optional.of(dataspaceEntity));
		given(dataspaceMapper.createFromEntity(any(DataspaceEntity.class))).willReturn(dataspaceModel);	
		
		Dataspace dataspace = dataspaceService.findById(1L);
        
		assertThat(dataspace).isNotNull();
        
	}
		
}
