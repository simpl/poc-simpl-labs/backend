package it.eng.simpl.labs.poc.backend.test.mapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import it.eng.simpl.labs.poc.entity.CategoryEntity;
import it.eng.simpl.labs.poc.entity.ComponentEntity;
import it.eng.simpl.labs.poc.mapper.CategoryMapper;
import it.eng.simpl.labs.poc.mapper.ComponentMapper;
import it.eng.simpl.labs.poc.model.Category;
import it.eng.simpl.labs.poc.model.Component;

@ExtendWith(MockitoExtension.class)
class CategoryMapperTest {
	
	@Mock
	private ComponentMapper componentMapper;
	
	@InjectMocks
	private CategoryMapper categoryMapper;
	private CategoryEntity categoryEntity;
	private Category category;
	private Stream<ComponentEntity> componentEntityStream;
	private Stream<Component> componentStream;
	private List<ComponentEntity> componentEntityList;
	private List<Component> componentList;
	
	@BeforeEach
	public void setup(){
		Component component=Component.builder().id(1L).build();
		componentList=Arrays.asList(component);
		componentStream=componentList.stream();
		
		ComponentEntity componentEntity=new ComponentEntity();
		componentEntity.setId(1L);	
		componentEntityList=Arrays.asList(componentEntity);
		componentEntityStream=componentEntityList.stream();
		
		category=Category.builder().id(1L).build();
		category.setComponentList(componentList);
		
		categoryEntity=new CategoryEntity();
		categoryEntity.setId(1L);	
		categoryEntity.setComponentList(componentEntityList);
	}
	
	@DisplayName("Test for CategoryMapper.createFromDto")
    @Test
    void givenCategoryDto_thenReturnCategoryEntity(){
		given(componentMapper.createFromDtos(any(List.class)))
		.willReturn(componentEntityStream);
		
		CategoryEntity returnedEntity=categoryMapper.createFromDto(category);
		assertThat(returnedEntity).isNotNull().hasFieldOrPropertyWithValue("id", category.getId());
	}
	
	@DisplayName("Test for CategoryMapper.createFromEntity")
    @Test
    void givenCategoryEntity_thenReturnCategoryDto(){
		given(componentMapper.createFromEntities(any(List.class)))
		.willReturn(componentStream);
		
		Category returnedDto=categoryMapper.createFromEntity(categoryEntity);
		assertThat(returnedDto).isNotNull().hasFieldOrPropertyWithValue("id", categoryEntity.getId());
	}
	
}
