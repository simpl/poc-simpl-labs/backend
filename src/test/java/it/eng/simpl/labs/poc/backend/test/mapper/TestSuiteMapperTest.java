package it.eng.simpl.labs.poc.backend.test.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import it.eng.simpl.labs.poc.entity.TestSuiteEntity;
import it.eng.simpl.labs.poc.mapper.TestSuiteMapper;
import it.eng.simpl.labs.poc.model.TestSuite;

@ExtendWith(MockitoExtension.class)
class TestSuiteMapperTest {
	@InjectMocks
	private TestSuiteMapper testSuiteMapper;
	
	private TestSuiteEntity testSuiteEntity;
	
	@BeforeEach
	public void setup(){
		testSuiteEntity=new TestSuiteEntity();
		testSuiteEntity.setId("TS1");
	}
	
	@DisplayName("Test for TestSuiteMapper.createFromEntity")
    @Test
    void givenTestSuiteEntity_thenReturnTestSuiteDto(){
		TestSuite returnedDto=testSuiteMapper.createFromEntity(testSuiteEntity);
		assertThat(returnedDto).isNotNull().hasFieldOrPropertyWithValue("id", testSuiteEntity.getId());
	}
	
}
