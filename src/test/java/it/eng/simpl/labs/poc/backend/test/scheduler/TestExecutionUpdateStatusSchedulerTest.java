package it.eng.simpl.labs.poc.backend.test.scheduler;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import it.eng.simpl.labs.poc.scheduler.TestExecutionUpdateStatusScheduler;
import it.eng.simpl.labs.poc.service.interf.ITestSessionsStatusUpdateService;

@ExtendWith(MockitoExtension.class)
class TestExecutionUpdateStatusSchedulerTest {
	@InjectMocks
	private TestExecutionUpdateStatusScheduler testExecutionUpdateStatusScheduler;
	
	@Mock
	private ITestSessionsStatusUpdateService testSessionsStatusUpdateService;
	
	
	@DisplayName("Test for TestExecutionUpdateStatusScheduler.run")
    @Test
    void assertRunWithoutProblems(){
		testExecutionUpdateStatusScheduler.run();
	}
}
