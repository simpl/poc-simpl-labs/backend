package it.eng.simpl.labs.poc.backend.test.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import it.eng.simpl.labs.poc.utils.ItbConfigurationUtils;

@ExtendWith(MockitoExtension.class)
class ItbConfigurationUtilsTest {

	@DisplayName("Test for ItbConfigurationUtils.getTestCaseStepResult")
	@Test
	void itbConfigurationUtilsgetTestCaseStepResult() throws IOException {
		
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream inputStream = classLoader.getResourceAsStream("TestCaseOverviewReport.xml");
		String data = readFromInputStream(inputStream);
		
		String localResult = ItbConfigurationUtils.getTestCaseStepResult(data, "1");
		
		assertNotNull(localResult);

	}
	
	@DisplayName("Test for ItbConfigurationUtils.getTestCaseStepResult - KO")
	@Test
	void itbConfigurationUtilsgetTestCaseStepResultKO() {
		
		assertThrows(Exception.class, () -> {
			ItbConfigurationUtils.getTestCaseStepResult("wrong_xml_data", "1");
		});
	
	}
	
	private String readFromInputStream(InputStream inputStream) throws IOException {
		StringBuilder resultStringBuilder = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
			String line;
			while ((line = br.readLine()) != null) {
				resultStringBuilder.append(line).append("\n");
			}
		}
		return resultStringBuilder.toString();
	}

}