package it.eng.simpl.labs.poc.backend.test.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.BDDMockito.given;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import it.eng.simpl.labs.poc.entity.CustomComponentEntity;
import it.eng.simpl.labs.poc.mapper.CustomComponentMapper;
import it.eng.simpl.labs.poc.model.Category;
import it.eng.simpl.labs.poc.model.Component;
import it.eng.simpl.labs.poc.model.CustomComponent;
import it.eng.simpl.labs.poc.model.Dataspace;
import it.eng.simpl.labs.poc.model.Node;
import it.eng.simpl.labs.poc.model.User;
import it.eng.simpl.labs.poc.repository.CustomComponentRepository;
import it.eng.simpl.labs.poc.service.CustomComponentService;
import it.eng.simpl.labs.poc.service.DataspaceService;

@ExtendWith(MockitoExtension.class)
class CustomComponentServiceTest {
	
	private Component component;
	private CustomComponent customComponent;
	private Dataspace dataspace;
	
	private CustomComponentEntity customComponentEntity;

	@Mock
	private CustomComponentRepository customComponentRepository;

	@Mock
	private CustomComponentMapper customComponentMapper;
	
	@Mock
	private DataspaceService dataspaceService;

	@InjectMocks
	private CustomComponentService customComponentService;

	private Stream<CustomComponent> customComponentStream;
	
	private List<CustomComponent> customComponentList;
	
	private User user;
	
	@BeforeEach
	public void setup() {
		
		user=User.builder().email("test@test.com").username("testUser").build();
		
		component=Component.builder().customComponentId(1L).build();
		customComponent = CustomComponent.builder().id(1L).build();
		customComponentEntity=new CustomComponentEntity();
		customComponentEntity.setId(1L);
		customComponentList=new ArrayList<>();
		customComponentList.add(customComponent);
		customComponentStream=customComponentList.stream();
		
		Category category = Category.builder().componentList(Arrays.asList(component)).build();
		Node node = Node.builder().categoryList(Arrays.asList(category)).build();
		dataspace = Dataspace.builder().nodeList(Arrays.asList(node)).build();
		
	}

	@DisplayName("Test for CustomComponentService.findAll")
	@Test
	void testfindAllCustomComponentList() {

		given(customComponentMapper.createFromEntities(anyList())).willReturn(customComponentStream);

		assertThat(customComponentService.findAll()).isNotNull().hasSizeGreaterThan(0);

	}
	
	@DisplayName("Test for CustomComponentService.findById")
	@Test
	void givenComponentId_whenFind_thenReturnComponent() {
		given(customComponentRepository.findById(1L)).willReturn(Optional.of(customComponentEntity));
		given(customComponentMapper.createFromEntity(any(CustomComponentEntity.class))).willReturn(customComponent);
		CustomComponent customComponentReturned=customComponentService.findById(1L);
		assertThat(customComponentReturned)
			.isNotNull()
			.hasFieldOrPropertyWithValue("id", 1L);
	}
	
	@DisplayName("Test for CustomComponentService.getDataspaces")
	@Test
	void givenComponentId_whenFind_thenReturnDataspaceList() {
		given(dataspaceService.findAll(any(User.class))).willReturn(Arrays.asList(dataspace));
		List<Dataspace> dataspaceListReturned=customComponentService.getDataspaces(1L, user);
		
		assertThat(dataspaceListReturned)
			.isNotNull()
			.hasSize(1);
	}

}
