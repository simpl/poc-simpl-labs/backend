package it.eng.simpl.labs.poc.backend.test.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.Node;
import io.fabric8.kubernetes.api.model.NodeList;
import io.fabric8.kubernetes.api.model.NodeStatus;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.ObjectMetaBuilder;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.PodList;
import io.fabric8.kubernetes.api.model.PodSpec;
import io.fabric8.kubernetes.api.model.Quantity;
import io.fabric8.kubernetes.api.model.ResourceRequirements;
import io.fabric8.kubernetes.api.model.Status;
import io.fabric8.kubernetes.api.model.metrics.v1beta1.ContainerMetrics;
import io.fabric8.kubernetes.api.model.metrics.v1beta1.NodeMetrics;
import io.fabric8.kubernetes.api.model.metrics.v1beta1.PodMetrics;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientException;
import io.fabric8.kubernetes.client.dsl.MetricAPIGroupDSL;
import io.fabric8.kubernetes.client.dsl.MixedOperation;
import io.fabric8.kubernetes.client.dsl.PodMetricOperation;
import io.fabric8.kubernetes.client.dsl.Resource;
import io.fabric8.kubernetes.client.dsl.internal.core.v1.PodOperationsImpl;
import it.eng.simpl.labs.poc.entity.ComponentEntity;
import it.eng.simpl.labs.poc.entity.CurrentMetricsEntity;
import it.eng.simpl.labs.poc.entity.HistoricalMetricsEntity;
import it.eng.simpl.labs.poc.exception.GenericException;
import it.eng.simpl.labs.poc.repository.ComponentRepository;
import it.eng.simpl.labs.poc.service.K8sClusterManagementService;

@ExtendWith(MockitoExtension.class)
class K8sClusterManagementServiceTest {

	@InjectMocks
	private K8sClusterManagementService k8sClusterManagementService;

	@Mock
	private KubernetesClient kubernetesClient;

	@Mock
	private ComponentRepository componentRepository;

	private BigDecimal allocatedStorage;	
	private BigDecimal allocatableStorage;
	private	BigDecimal minStorageFile;
	private String allocatableMemory;
	private String namespace;
	private String podName;
	private String beContainerName;
	private String feContainerName;
	private String podIdentifierLabel;
	private String podIdentifierValue;
	private Map<String, Quantity> limitsMap;
	private Map<String, Quantity> allocatableMemoryMap;

	private Pod pod;
	private Pod wrongPod;

	@BeforeEach
	public void setup(){

		allocatedStorage = new BigDecimal("500");
		allocatableStorage = new BigDecimal("1000");
		minStorageFile = new BigDecimal("100");

		namespace = "default";
		podName = "test-pod";
		beContainerName = "test-container-be-component";
		feContainerName = "test-container-fe-component";
		podIdentifierLabel = "pod-identifier";
		podIdentifierValue = "1-2-3";
		limitsMap = Map.of(
				"cpu", Quantity.parse("200m"),
				"memory", Quantity.parse("512Mi")
				);
		allocatableMemory = "536870912";
		allocatableMemoryMap = Map.of(
				"memory", Quantity.parse("512Mi")
				);		

		long componentId = 3L;
		ComponentEntity componentEntity = new ComponentEntity();
		componentEntity.setId(componentId);
		lenient().when(componentRepository.findByCategoryIdOrderByTypeAsc(componentId)).thenReturn(Arrays.asList(componentEntity));

		MockitoAnnotations.openMocks(this);

		ReflectionTestUtils.setField(k8sClusterManagementService, "allocatableStorage", allocatableStorage);
		ReflectionTestUtils.setField(k8sClusterManagementService, "maxStorageFile", allocatedStorage);
		ReflectionTestUtils.setField(k8sClusterManagementService, "minStorageFile", minStorageFile);
		ReflectionTestUtils.setField(k8sClusterManagementService, "nameSpace", namespace);
		ReflectionTestUtils.setField(k8sClusterManagementService, "kubeconfigFile", namespace);

		pod = new Pod();
		pod.setMetadata(new ObjectMeta());
		pod.getMetadata().setName(podName);
		pod.getMetadata().setLabels(Map.of(podIdentifierLabel, podIdentifierValue));
		PodSpec podSpec = new PodSpec();
		Container beContainer = new Container();
		beContainer.setName(beContainerName);
		ResourceRequirements resource = new ResourceRequirements();
		resource.setLimits(limitsMap);
		beContainer.setResources(resource);
		Container feContainer = new Container();
		feContainer.setName(feContainerName);
		resource.setLimits(limitsMap);
		feContainer.setResources(resource);

		podSpec.setContainers(Arrays.asList(beContainer, feContainer));
		pod.setSpec(podSpec);

		wrongPod = new Pod();
		wrongPod.setMetadata(new ObjectMeta());

	}

	@Test
	@DisplayName("Test for k8sClusterManagementService.getHistoricalMetrics - Success - No Client")
	void getHistoricalMetricsSuccessNoClient() {
		
		ReflectionTestUtils.setField(k8sClusterManagementService, "kubernetesClient", null);
	    assertDoesNotThrow(() -> {
	    	k8sClusterManagementService.getHistoricalMetrics();
	    });	

	}
	
	@Test
	@DisplayName("Test for k8sClusterManagementService.getHistoricalMetrics - Success - no Support")
	void getHistoricalMetricsSuccessNoSupport() {
		
		when(kubernetesClient.supports(NodeMetrics.class)).thenReturn(false);
		List<HistoricalMetricsEntity> result = k8sClusterManagementService.getHistoricalMetrics();
		assertTrue(result.isEmpty());
		
	}

	@Test
	@DisplayName("Test for k8sClusterManagementService.getHistoricalMetrics - Success")
	void getHistoricalMetricsSuccess() {

		// Support
		when(kubernetesClient.supports(NodeMetrics.class)).thenReturn(true);

		// Pods list
		PodOperationsImpl podOperations = mock(PodOperationsImpl.class);
		when(kubernetesClient.pods()).thenReturn(podOperations);
		PodOperationsImpl podOperationsInNamespace = mock(PodOperationsImpl.class);
		when(podOperations.inNamespace(namespace)).thenReturn(podOperationsInNamespace);
		PodList podList = mock(PodList.class);
		when(podOperationsInNamespace.list()).thenReturn(podList);

		// Top
		when(podList.getItems()).thenReturn(List.of(pod, wrongPod));
		MetricAPIGroupDSL topClient = mock(MetricAPIGroupDSL.class);
		when(kubernetesClient.top()).thenReturn(topClient);
		PodMetricOperation podOps = mock(PodMetricOperation.class);
		when(topClient.pods()).thenReturn(podOps);

		// Metrics
		PodMetrics podMetrics = mock(PodMetrics.class);
		when(podOps.metrics(anyString(), anyString())).thenReturn(podMetrics);
		ContainerMetrics beContainerMetrics = mock(ContainerMetrics.class);
		lenient().when(beContainerMetrics.getName()).thenReturn(beContainerName);
		lenient().when(beContainerMetrics.getUsage()).thenReturn(limitsMap);
		ContainerMetrics feContainerMetrics = mock(ContainerMetrics.class);
		when(feContainerMetrics.getName()).thenReturn(feContainerName);
		when(feContainerMetrics.getUsage()).thenReturn(limitsMap);
		when(podMetrics.getContainers()).thenReturn(List.of(feContainerMetrics));

		List<HistoricalMetricsEntity> result = k8sClusterManagementService.getHistoricalMetrics();

		assertFalse(result.isEmpty());
		assertEquals(1, result.size());
		HistoricalMetricsEntity metricsEntity = result.get(0);
		assertEquals(1L, metricsEntity.getDataspaceId());
		assertEquals(2L, metricsEntity.getNodeId());
		assertEquals(3L, metricsEntity.getCategoryId());
		assertEquals(allocatedStorage, metricsEntity.getStorageAllocated()); 
	}
	
	@Test
	@DisplayName("Test for k8sClusterManagementService.getHistoricalMetrics - Error - No Pod Metrics")
	void testGetHistoricalMetricsErrorNoPodMetrics() {

		// Support
		when(kubernetesClient.supports(NodeMetrics.class)).thenReturn(true);

		// Pods list
		PodOperationsImpl podOperations = mock(PodOperationsImpl.class);
		when(kubernetesClient.pods()).thenReturn(podOperations);
		PodOperationsImpl podOperationsInNamespace = mock(PodOperationsImpl.class);
		when(podOperations.inNamespace(namespace)).thenReturn(podOperationsInNamespace);
		PodList podList = mock(PodList.class);
		when(podOperationsInNamespace.list()).thenReturn(podList);

		// Top
		when(podList.getItems()).thenReturn(List.of(pod, wrongPod));
		MetricAPIGroupDSL topClient = mock(MetricAPIGroupDSL.class);
		when(kubernetesClient.top()).thenReturn(topClient);
		PodMetricOperation podOps = mock(PodMetricOperation.class);
		when(topClient.pods()).thenReturn(podOps);

		// Metrics
		when(podOps.metrics(anyString(), anyString())).thenThrow(new KubernetesClientException("error message"));
		List<HistoricalMetricsEntity> result = k8sClusterManagementService.getHistoricalMetrics();
		assertTrue(result.isEmpty());

	}
	
	@Test
	@DisplayName("Test for k8sClusterManagementService.getHistoricalMetrics - Error - No Component")
	void testGetHistoricalMetricsErrorNoComponent() {
		
		pod.getSpec().getContainers().getFirst().setName("wrong-name");

		// Support
		when(kubernetesClient.supports(NodeMetrics.class)).thenReturn(true);

		// Pods list
		PodOperationsImpl podOperations = mock(PodOperationsImpl.class);
		when(kubernetesClient.pods()).thenReturn(podOperations);
		PodOperationsImpl podOperationsInNamespace = mock(PodOperationsImpl.class);
		when(podOperations.inNamespace(namespace)).thenReturn(podOperationsInNamespace);
		PodList podList = mock(PodList.class);
		when(podOperationsInNamespace.list()).thenReturn(podList);

		// Top
		when(podList.getItems()).thenReturn(List.of(pod, wrongPod));
		MetricAPIGroupDSL topClient = mock(MetricAPIGroupDSL.class);
		when(kubernetesClient.top()).thenReturn(topClient);
		PodMetricOperation podOps = mock(PodMetricOperation.class);
		when(topClient.pods()).thenReturn(podOps);

		// Metrics
		PodMetrics podMetrics = mock(PodMetrics.class);
		when(podOps.metrics(anyString(), anyString())).thenReturn(podMetrics);
		ContainerMetrics beContainerMetrics = mock(ContainerMetrics.class);
		lenient().when(beContainerMetrics.getName()).thenReturn(beContainerName);
		lenient().when(beContainerMetrics.getUsage()).thenReturn(limitsMap);
		ContainerMetrics feContainerMetrics = mock(ContainerMetrics.class);
		when(feContainerMetrics.getName()).thenReturn("wrong-name");
		when(feContainerMetrics.getUsage()).thenReturn(limitsMap);
		when(podMetrics.getContainers()).thenReturn(List.of(feContainerMetrics));

		assertThrows(GenericException.class, () -> {
			k8sClusterManagementService.getHistoricalMetrics();
		});
		
	}
	
	@Test
	@DisplayName("Test for k8sClusterManagementService.getHistoricalMetrics - Error - No Limits")
	void testGetHistoricalMetricsErrorNoLimits() {
				
		pod.getSpec().getContainers().getFirst().getResources().setLimits(Map.of("wrongKey", Quantity.parse("200m")));

		// Support
		when(kubernetesClient.supports(NodeMetrics.class)).thenReturn(true);

		// Pods list
		PodOperationsImpl podOperations = mock(PodOperationsImpl.class);
		when(kubernetesClient.pods()).thenReturn(podOperations);
		PodOperationsImpl podOperationsInNamespace = mock(PodOperationsImpl.class);
		when(podOperations.inNamespace(namespace)).thenReturn(podOperationsInNamespace);
		PodList podList = mock(PodList.class);
		when(podOperationsInNamespace.list()).thenReturn(podList);

		// Top
		when(podList.getItems()).thenReturn(List.of(pod, wrongPod));
		MetricAPIGroupDSL topClient = mock(MetricAPIGroupDSL.class);
		when(kubernetesClient.top()).thenReturn(topClient);
		PodMetricOperation podOps = mock(PodMetricOperation.class);
		when(topClient.pods()).thenReturn(podOps);

		// Metrics
		PodMetrics podMetrics = mock(PodMetrics.class);
		when(podOps.metrics(anyString(), anyString())).thenReturn(podMetrics);
		ContainerMetrics beContainerMetrics = mock(ContainerMetrics.class);
		lenient().when(beContainerMetrics.getName()).thenReturn(beContainerName);
		lenient().when(beContainerMetrics.getUsage()).thenReturn(limitsMap);
		ContainerMetrics feContainerMetrics = mock(ContainerMetrics.class);
		when(feContainerMetrics.getName()).thenReturn(feContainerName);
		when(feContainerMetrics.getUsage()).thenReturn(limitsMap);
		when(podMetrics.getContainers()).thenReturn(List.of(feContainerMetrics));

		assertThrows(GenericException.class, () -> {
			k8sClusterManagementService.getHistoricalMetrics();
		});
		
	}
	
	@Test
	@DisplayName("Test for k8sClusterManagementService.getAllocatableResources - Storage - Success")
	void getAllocatableResourcesStorageSuccess() {

		BigDecimal result = k8sClusterManagementService.getAllocatableResources("storage");

		assertEquals(result, allocatableStorage);

	}

	@Test
	@DisplayName("Test for k8sClusterManagementService.getAllocatableResources - CPU/Memory - Success")
	void getAllocatableResourcesCpuMemorySuccess() {

		when(kubernetesClient.supports(NodeMetrics.class)).thenReturn(true);

		MixedOperation<Node, NodeList, Resource<Node>> nodeOperation = mock(MixedOperation.class);

		NodeList nodeList = new NodeList();
		Node node = new Node();
		NodeStatus nodeStatus = new NodeStatus();
		nodeStatus.setAllocatable(allocatableMemoryMap);
		node.setStatus(nodeStatus);
		node.setMetadata(new ObjectMetaBuilder().withName("test-node").build());
		nodeList.setItems(List.of(node));

		when(kubernetesClient.nodes()).thenReturn(nodeOperation);

		when(nodeOperation.list()).thenReturn(nodeList);

		BigDecimal result = k8sClusterManagementService.getAllocatableResources("memory");

		assertEquals(result, new BigDecimal(allocatableMemory));
	}

	@Test
	@DisplayName("Test for k8sClusterManagementService.getAllocatableResources - Error - No Client")
	void getAllocatableResourcesErrorNoClient() {
		
		ReflectionTestUtils.setField(k8sClusterManagementService, "kubernetesClient", null);

	    assertDoesNotThrow(() -> {
			k8sClusterManagementService.getAllocatableResources("memory");
	    });	

	}
	
	@Test
	@DisplayName("Test for k8sClusterManagementService.getAllocatableResources - Error - No Support")
	void getAllocatableResourcesErrorNoSupport() {

		when(kubernetesClient.supports(NodeMetrics.class)).thenReturn(false);
		
	    assertDoesNotThrow(() -> {
			k8sClusterManagementService.getAllocatableResources("memory");
	    });	

	}
	
	@Test
	@DisplayName("Test for k8sClusterManagementService.getCurrentMetrics")
	void getCurrentMetrics() {

		// Support
		when(kubernetesClient.supports(NodeMetrics.class)).thenReturn(true);

		// Pods list
		PodOperationsImpl podOperations = mock(PodOperationsImpl.class);
		when(kubernetesClient.pods()).thenReturn(podOperations);
		PodOperationsImpl podOperationsInNamespace = mock(PodOperationsImpl.class);
		when(podOperations.inNamespace(namespace)).thenReturn(podOperationsInNamespace);
		PodList podList = mock(PodList.class);
		when(podOperationsInNamespace.list()).thenReturn(podList);

		// Top
		when(podList.getItems()).thenReturn(List.of(pod, wrongPod));
		MetricAPIGroupDSL topClient = mock(MetricAPIGroupDSL.class);
		when(kubernetesClient.top()).thenReturn(topClient);
		PodMetricOperation podOps = mock(PodMetricOperation.class);
		when(topClient.pods()).thenReturn(podOps);

		// Metrics
		PodMetrics podMetrics = mock(PodMetrics.class);
		when(podOps.metrics(anyString(), anyString())).thenReturn(podMetrics);
		ContainerMetrics containerMetrics = mock(ContainerMetrics.class);
		when(containerMetrics.getName()).thenReturn(beContainerName);
		when(containerMetrics.getUsage()).thenReturn(limitsMap);
		when(podMetrics.getContainers()).thenReturn(List.of(containerMetrics));

		List<CurrentMetricsEntity> result = k8sClusterManagementService.getCurrentMetrics();

		assertFalse(result.isEmpty());
		assertEquals(1, result.size());

	}

	@Test
	@DisplayName("Test for k8sClusterManagementService.init")
	void init() {

		MockedStatic<Files> mockedFiles = Mockito.mockStatic(Files.class);

		MockedStatic<Config> mockedConfig = Mockito.mockStatic(Config.class);

		mockedFiles.when(() -> Files.readString(any(Path.class))).thenReturn("mocked kubeconfig content");

		Config mockConfig = mock(Config.class);
		mockedConfig.when(() -> Config.fromKubeconfig("mocked kubeconfig content")).thenReturn(mockConfig);

		ReflectionTestUtils.invokeMethod(k8sClusterManagementService, "init");

		ReflectionTestUtils.setField(k8sClusterManagementService, "kubeconfigFile", null);

	    assertDoesNotThrow(() -> {
			ReflectionTestUtils.invokeMethod(k8sClusterManagementService, "init");
	    });	
	}

}