package it.eng.simpl.labs.poc.backend.test.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.marcnuri.helm.InstallCommand;
import com.marcnuri.helm.UninstallCommand;

import it.eng.simpl.labs.poc.entity.CategoryEntity;
import it.eng.simpl.labs.poc.entity.DataspaceEntity;
import it.eng.simpl.labs.poc.enumeration.CategoryTypeEnum;
import it.eng.simpl.labs.poc.enumeration.ComponentTypeEnum;
import it.eng.simpl.labs.poc.enumeration.NodeTypeEnum;
import it.eng.simpl.labs.poc.model.Category;
import it.eng.simpl.labs.poc.model.Component;
import it.eng.simpl.labs.poc.model.Dataspace;
import it.eng.simpl.labs.poc.model.InstallCommandWrapper;
import it.eng.simpl.labs.poc.model.Node;
import it.eng.simpl.labs.poc.model.UninstallCommandWrapper;
import it.eng.simpl.labs.poc.repository.CategoryRepository;
import it.eng.simpl.labs.poc.repository.ComponentRepository;
import it.eng.simpl.labs.poc.repository.DataspaceRepository;
import it.eng.simpl.labs.poc.service.DeploymentService;

@ExtendWith(MockitoExtension.class)
class DeploymentServiceTest {

	@InjectMocks
	private DeploymentService deploymentService;

	@Mock
	private DataspaceRepository dataspaceRepository;

	@Mock
	private CategoryRepository categoryRepository;

	@Mock
	private ComponentRepository componentRepository;

	@Mock
	private InstallCommandWrapper installCommandWrapper;

	@Mock
	private UninstallCommandWrapper uninstallCommandWrapper;

	private Dataspace dataspace;

	private DataspaceEntity entity;

	@BeforeEach
	public void setup(){

		dataspace = Dataspace.builder().id(1L).build();

		List<Component> componentList = new ArrayList<>();
		componentList.add(Component.builder().id(1L).customImage("custom-image1:tag1").type(ComponentTypeEnum.BE_COMPONENT).build());
		componentList.add(Component.builder().id(1L).customImage("custom-image1:tag1").type(ComponentTypeEnum.FE_COMPONENT).build());

		List<Category> categoryList = new ArrayList<>();
		Category category = Category.builder()
				.id(1L)
				.type(CategoryTypeEnum.APPLICATION_SHARING)
				.componentList(componentList)
				.build();
		categoryList.add(category);
		Node node = Node.builder().build();
		node.setId(1L);
		node.setCategoryList(categoryList);
		List<Node> nodeList = new ArrayList<>();
		nodeList.add(node);

		dataspace.setNodeList(nodeList);
		
		entity = new DataspaceEntity();
		entity.setId(1L);

		ReflectionTestUtils.setField(deploymentService, "chartDir", "test-chart-dir");
		ReflectionTestUtils.setField(deploymentService, "gaChart", "test-ga-chart");
		ReflectionTestUtils.setField(deploymentService, "kubeconfigFile", "test-kubeconfigFile");
		ReflectionTestUtils.setField(deploymentService, "namespace", "test-namespace");
		ReflectionTestUtils.setField(deploymentService, "sleepTime", 1L);

	}

	@DisplayName("Test for DeploymentService.initDataspaceCreation")
	@Test
	void initDataspaceCreation(){
		
		CategoryEntity categoryEntity = new CategoryEntity();

		InstallCommand mockInstallCommand = mock(InstallCommand.class);

		when(installCommandWrapper.init(anyString(), anyString(), anyString())).thenReturn(mockInstallCommand);

		when(installCommandWrapper.set(anyString(), anyString())).thenReturn(mockInstallCommand);

		when(installCommandWrapper.install()).thenReturn(mockInstallCommand);

		when(dataspaceRepository.findById(1L)).thenReturn(Optional.of(entity));

		when(categoryRepository.findByNodeIdAndType(1L, CategoryTypeEnum.APPLICATION_SHARING)).thenReturn(categoryEntity);

		when(installCommandWrapper.setProperty(anyString(), anyString())).thenReturn(mockInstallCommand);

		when(categoryRepository.saveAndFlush(any())).thenReturn(categoryEntity);

		dataspace.getNodeList().getFirst().setType(NodeTypeEnum.GOVERNANCE);
	    assertDoesNotThrow(() -> {
	    	deploymentService.initDataspaceCreation(dataspace);
	    });	

		dataspace.getNodeList().getFirst().setType(NodeTypeEnum.APPLICATION_PROVIDER);
	    assertDoesNotThrow(() -> {
	    	deploymentService.initDataspaceCreation(dataspace);
	    });	

		dataspace.getNodeList().getFirst().setType(NodeTypeEnum.DATA_PROVIDER);
	    assertDoesNotThrow(() -> {
	    	deploymentService.initDataspaceCreation(dataspace);
	    });	

		dataspace.getNodeList().getFirst().setType(NodeTypeEnum.INFRASTRUCTURE_PROVIDER);
	    assertDoesNotThrow(() -> {
	    	deploymentService.initDataspaceCreation(dataspace);
	    });	

		dataspace.getNodeList().getFirst().setType(NodeTypeEnum.CONSUMER);		
	    assertDoesNotThrow(() -> {
	    	deploymentService.initDataspaceCreation(dataspace);
	    });	
	    
	}
	
	@DisplayName("Test for DeploymentService.initDataspaceCreation - Error")
	@Test
	void initDataspaceCreationError(){
		
		InstallCommand mockInstallCommand = mock(InstallCommand.class);

		when(installCommandWrapper.init(anyString(), anyString(), anyString())).thenReturn(mockInstallCommand);

		when(installCommandWrapper.set(anyString(), anyString())).thenReturn(mockInstallCommand);

		when(dataspaceRepository.findById(1L)).thenReturn(Optional.of(entity));

		when(categoryRepository.findByNodeIdAndType(1L, CategoryTypeEnum.APPLICATION_SHARING)).thenReturn(null);

		dataspace.getNodeList().getFirst().setType(NodeTypeEnum.GOVERNANCE);
		
	    assertDoesNotThrow(() -> {
	    	deploymentService.initDataspaceCreation(dataspace);
	    });	

	    
	}

	@DisplayName("Test for DeploymentService.initDataspaceDeletion")
	@Test
	void initDataspaceDeletion(){

		UninstallCommand mockInstallCommand = mock(UninstallCommand.class);

		when(uninstallCommandWrapper.init(anyString())).thenReturn(mockInstallCommand);

		when(uninstallCommandWrapper.set(anyString(), anyString())).thenReturn(mockInstallCommand);

		when(uninstallCommandWrapper.uninstall()).thenReturn(mockInstallCommand);

		when(dataspaceRepository.findById(1L)).thenReturn(Optional.of(entity));

		lenient().when(categoryRepository.save(any())).thenReturn(entity);
		
		doNothing().when(componentRepository).deleteById(anyLong());
		
		doNothing().when(componentRepository).flush();
		
		doNothing().when(dataspaceRepository).delete(any(DataspaceEntity.class));
		
		doNothing().when(dataspaceRepository).flush();

		dataspace.getNodeList().getFirst().setType(NodeTypeEnum.GOVERNANCE);
	    assertDoesNotThrow(() -> {
			deploymentService.initDataspaceDeletion(dataspace);
	    });	

		dataspace.getNodeList().getFirst().setType(NodeTypeEnum.APPLICATION_PROVIDER);
	    assertDoesNotThrow(() -> {
			deploymentService.initDataspaceDeletion(dataspace);
	    });	

		dataspace.getNodeList().getFirst().setType(NodeTypeEnum.DATA_PROVIDER);
	    assertDoesNotThrow(() -> {
			deploymentService.initDataspaceDeletion(dataspace);
	    });	

		dataspace.getNodeList().getFirst().setType(NodeTypeEnum.INFRASTRUCTURE_PROVIDER);
	    assertDoesNotThrow(() -> {
			deploymentService.initDataspaceDeletion(dataspace);
	    });	

		dataspace.getNodeList().getFirst().setType(NodeTypeEnum.CONSUMER);
	    assertDoesNotThrow(() -> {
			deploymentService.initDataspaceDeletion(dataspace);
	    });		

	}
	
	@DisplayName("Test for DeploymentService.initDataspaceDeletion - Error")
	@Test
	void initDataspaceDeletionError(){

		when(dataspaceRepository.findById(1L)).thenReturn(null);

	    assertDoesNotThrow(() -> {
			deploymentService.initDataspaceDeletion(dataspace);
	    });		

	}


}
