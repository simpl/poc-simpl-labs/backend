package it.eng.simpl.labs.poc.entity;

import it.eng.simpl.labs.poc.enumeration.ComponentTypeEnum;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class ComponentEntity extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	@Min(0)
	@Max(9223372036854775807l)
	private Long id;
	
	@Enumerated
	private ComponentTypeEnum type;
	
	@Column
	private String customImage;

	@ManyToOne
	@JoinColumn(name="category_id")
	private CategoryEntity category;

	@ManyToOne
	@JoinColumn(name="custom_component_id", insertable = false, updatable = false)
	private CustomComponentEntity customComponent;
		
	@Column(name="custom_component_id")
	private Long customComponentId;
}
