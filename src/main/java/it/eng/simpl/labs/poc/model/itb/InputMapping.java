package it.eng.simpl.labs.poc.model.itb;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class InputMapping {

  private List<String> testSuite = new ArrayList<>();

  private List<String> testCase = new ArrayList<>();

}

