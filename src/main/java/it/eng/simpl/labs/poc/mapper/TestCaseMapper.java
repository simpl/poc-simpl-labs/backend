package it.eng.simpl.labs.poc.mapper;

import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.eng.simpl.labs.poc.entity.TestCaseEntity;
import it.eng.simpl.labs.poc.entity.TestCaseExecutionEntity;
import it.eng.simpl.labs.poc.enumeration.TestStatusEnum;
import it.eng.simpl.labs.poc.model.TestCase;
import it.eng.simpl.labs.poc.model.TestCaseStep;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class TestCaseMapper extends BaseMapper<TestCase, TestCaseEntity> {

	@Autowired
	public void setDataSource(ObjectMapper mapper){
		this.mapper = mapper;
	}

	@Override
	public TestCaseEntity createFromDto(TestCase dto) {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public TestCase createFromEntity(TestCaseEntity entity) {

		TestCase dto = null;

		if (entity != null) {
			dto = TestCase.builder().build();
			copyProperties(entity, dto);

			dto.setId(entity.getId().getTestCaseId());

			try {
				List<TestCaseStep> categoryList = mapper.readValue(entity.getSteps(), new TypeReference<List<TestCaseStep>>(){});
				dto.setStepsList(categoryList);
			} catch (JsonProcessingException e) {
				log.error(ExceptionUtils.getStackTrace(e));
			} 
		}

		return dto;
	}

	public TestCase createFromExecutionEntity(TestCaseExecutionEntity entity) {

		TestCase dto = null;

		if (entity != null) {

			dto = TestCase.builder().build();

			copyProperties(entity, dto);

			dto.setId(entity.getTestCase().getId().getTestCaseId());
			dto.setName(entity.getTestCase().getName());
			dto.setDescription(entity.getTestCase().getDescription());
			dto.setVersion(entity.getTestCase().getVersion());

			if(TestStatusEnum.UNDEFINED.equals(entity.getStatus()) && entity.getEndDate() == null) {
				// When a test is STOPPED, ITB keeps it in UNDEFINED status and updates the end time
				dto.setStatus(TestStatusEnum.ONGOING.getValue());
			}else {			
				dto.setStatus(entity.getStatus() != null ? entity.getStatus().getValue() : TestStatusEnum.UNDEFINED.getValue());
			}

			dto.setTestSuiteId(entity.getTestSuiteExecution().getTestSuite().getId());
			dto.setTestSuiteDescription(entity.getTestSuiteExecution().getTestSuite().getDescription());
			dto.setTestSuiteLastUpdate(entity.getTestSuiteExecution().getTestSuite().getLastUpdateDate());

			try {
				List<TestCaseStep> testCaseStepsList = mapper.readValue(entity.getTestCase().getSteps(), new TypeReference<List<TestCaseStep>>(){});
				dto.setStepsList(testCaseStepsList);
			} catch (JsonProcessingException e) {
				log.error(ExceptionUtils.getStackTrace(e));
			} 
		}

		return dto;
	}

}
