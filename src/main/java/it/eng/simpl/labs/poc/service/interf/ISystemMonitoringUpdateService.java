/**
 * 
 */
package it.eng.simpl.labs.poc.service.interf;

import java.time.LocalDateTime;

/**
 * 
 */
public interface ISystemMonitoringUpdateService {
		
	/**
	 * Get Metrics from Kuernetes cluster and save them on local data base
	 */
	public void updateMetrics();
	
	/**
	 * Delete from local data base all metrics with no dataspace association+
	 */
	public void deleteOrphans();
	
	/**
	 * Delete from local data base all metrics older than @date
	 */
	public void deleteOldMetrics(LocalDateTime date);
	
	/**
	 * Delete from local data base all buffered metrics
	 */
	public void deleteBufferedMetrics();
	
	/**
	 * Get Metrics from Kuernetes cluster and save them on local buffer data base
	 */
	public void insertBufferedMetrics();

}
