package it.eng.simpl.labs.poc.model.itb;

import lombok.Data;

@Data
public class TestCaseOverviewReportStepsInnerStepReportReportsErrorInner {

	private String assertionID;

	private String description;

	private String location;

	private String test;

	private String type;

	private String value;

}
