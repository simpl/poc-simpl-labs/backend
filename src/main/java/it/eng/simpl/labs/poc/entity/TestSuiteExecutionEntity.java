package it.eng.simpl.labs.poc.entity;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class TestSuiteExecutionEntity extends BaseTestExecutionEntity {

	@Column(name = "test_execution_id")
	private Long testExecutionId;
	
	@ManyToOne
	@JoinColumn(name = "test_execution_id", insertable = false, updatable = false)
	private TestExecutionEntity testExecution;

	@OneToMany(mappedBy = "testSuiteExecution", cascade = CascadeType.ALL)
	private List<TestCaseExecutionEntity> testCaseExecutionList;
	
	@Column(name = "test_suite_id")
	private String testSuiteId;
	
	@ManyToOne
	@JoinColumn(name = "test_suite_id", nullable = false, insertable = false, updatable = false)
	private TestSuiteEntity testSuite;
	
	@Override
	public String toString() {
		return "TestSuiteExecutionEntity [id=" + id + ", status=" + status + ", startDate=" + startDate + ", endDate=" + endDate + "]";
	}

}
