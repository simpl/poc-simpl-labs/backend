package it.eng.simpl.labs.poc.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import it.eng.simpl.labs.poc.entity.DataspaceEntity;

@Repository
public interface DataspaceRepository extends BaseRepository<DataspaceEntity, Long>{

	List<DataspaceEntity> findAllByUserId(String username);

	DataspaceEntity findByIdAndUserId(Long dataspaceId, String username);
}
