package it.eng.simpl.labs.poc.model.itb;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class SessionStatusResponse implements ISessionResponse {

	private List<SessionStatus> sessions = new ArrayList<>();

}
