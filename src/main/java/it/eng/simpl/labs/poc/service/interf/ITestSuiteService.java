package it.eng.simpl.labs.poc.service.interf;

import java.util.List;

import it.eng.simpl.labs.poc.model.TestSuite;

public interface ITestSuiteService {

	/**
	 * @param specificationId
	 * @param componentId
	 * @return
	 */
	public List<TestSuite> findBySpecificationId(Long specificationId, Long componentId);
}
