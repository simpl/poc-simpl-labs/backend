package it.eng.simpl.labs.poc.service;

import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import io.fabric8.kubernetes.api.model.Container;
import io.fabric8.kubernetes.api.model.Node;
import io.fabric8.kubernetes.api.model.NodeList;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.Quantity;
import io.fabric8.kubernetes.api.model.ResourceRequirements;
import io.fabric8.kubernetes.api.model.metrics.v1beta1.ContainerMetrics;
import io.fabric8.kubernetes.api.model.metrics.v1beta1.NodeMetrics;
import io.fabric8.kubernetes.api.model.metrics.v1beta1.PodMetrics;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientBuilder;
import io.fabric8.kubernetes.client.KubernetesClientException;
import it.eng.simpl.labs.poc.entity.ComponentEntity;
import it.eng.simpl.labs.poc.entity.CurrentMetricsEntity;
import it.eng.simpl.labs.poc.entity.HistoricalMetricsEntity;
import it.eng.simpl.labs.poc.exception.GenericException;
import it.eng.simpl.labs.poc.repository.ComponentRepository;
import it.eng.simpl.labs.poc.service.interf.IK8sClusterManagementService;
import it.eng.simpl.labs.poc.utils.Constants;
import it.eng.simpl.labs.poc.utils.GenericUtils;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class K8sClusterManagementService implements IK8sClusterManagementService {

	@Value("${system.monitoring.storage.size.allocatable}")
	private BigDecimal allocatableStorage;

	@Value("${system.monitoring.storage.size.max}")
	private BigDecimal maxStorageFile;

	@Value("${system.monitoring.storage.size.min}")
	private BigDecimal minStorageFile;

	@Value("${system.monitoring.namespace}")
	private String nameSpace;

	@Value("${deployment.kubeconfig}")
	private String kubeconfigFile;
	
	private static final String METRICS_API_IS_NOT_ENABLED_IN_THE_CLUSTER = "Metrics API is not enabled in the cluster";

	private static final String THE_CONNECTION_TO_THE_CLUSTER_IS_NOT_AVAILABLE = "The connection to the cluster is not available";

	private static final String THE_LOCAL_COMPONENTS_CONFIGURATION_DOESN_T_MATCH_CLUSTER = "The local components configuration doesn't match cluster";

	private final ComponentRepository componentRepository;

	private KubernetesClient kubernetesClient;

	@PostConstruct
	private void init() {

		try {
			Config config = Config.fromKubeconfig(Files.readString(Path.of(kubeconfigFile)));
			kubernetesClient = new KubernetesClientBuilder().withConfig(config).build();
		} catch (Exception e) {
			log.error("Error connection to the cluster", e);
		}

	}

	@Override
	public List<HistoricalMetricsEntity> getHistoricalMetrics() {

		List<HistoricalMetricsEntity> historicalMetricsEntityList = new ArrayList<>();

		if(kubernetesClient != null) {

			if (kubernetesClient.supports(NodeMetrics.class)) {

				for(Pod pod : kubernetesClient.pods().inNamespace(nameSpace).list().getItems()) {

					String podName = pod.getMetadata().getName();

					Long dataSpaceId = null;
					Long nodeId = null;
					Long categoryId = null;

					Map<String, String> labels = pod.getMetadata().getLabels();
					String labelValue = labels.get(POD_IDENTIFIER);

					if(labelValue != null) {
						dataSpaceId = Long.parseLong(labelValue.split("-")[0]);
						nodeId = Long.parseLong(labelValue.split("-")[1]);
						categoryId = Long.parseLong(labelValue.split("-")[2]);
					}else {
						log.error("The POD [{}] doesn't have the [{}] label", podName, POD_IDENTIFIER);
						continue;
					}

					PodMetrics podMetrics = null;
					try {
						podMetrics = kubernetesClient.top().pods().metrics(nameSpace, podName);
					} catch (KubernetesClientException ex) {
						if (ex.getCode() == HttpURLConnection.HTTP_NOT_FOUND) {
							log.info("Pod metrics not yet available for namespace: {}", nameSpace);
						} else {
							log.error("Error retrieving Pod metrics for namespace: {}", nameSpace, ex);
						}
						break;
					}

					for(ContainerMetrics containerMetrics : podMetrics.getContainers()) {

						HistoricalMetricsEntity historicalMetricsEntity = new HistoricalMetricsEntity(); 

						historicalMetricsEntity.setDataspaceId(dataSpaceId);
						historicalMetricsEntity.setNodeId(nodeId);
						historicalMetricsEntity.setCategoryId(categoryId);
						historicalMetricsEntity.setCpuUsed(containerMetrics.getUsage().get(CPU).getNumericalAmount());
						historicalMetricsEntity.setMemoryUsed(containerMetrics.getUsage().get(MEMORY).getNumericalAmount());
						historicalMetricsEntity.setStorageUsed(GenericUtils.getRandomBigDecimalBetween(minStorageFile, maxStorageFile));
						historicalMetricsEntity.setStorageAllocated(maxStorageFile);

						for (Container container : pod.getSpec().getContainers()) {

							if(container.getName().equals(containerMetrics.getName())) {

								// Set component Id
								List<ComponentEntity> componentEntityList = componentRepository.findByCategoryIdOrderByTypeAsc(categoryId);
								if(componentEntityList != null && !componentEntityList.isEmpty()) {
									if (container.getName().endsWith(BE_COMPONENT)) {
										historicalMetricsEntity.setComponentId(componentEntityList.getFirst().getId());
									} else if (container.getName().endsWith(FE_COMPONENT)) {
										historicalMetricsEntity.setComponentId(componentEntityList.getLast().getId());
									} else {
										throw new GenericException(THE_LOCAL_COMPONENTS_CONFIGURATION_DOESN_T_MATCH_CLUSTER);
									}
								} else {
									throw new GenericException(THE_LOCAL_COMPONENTS_CONFIGURATION_DOESN_T_MATCH_CLUSTER);
								}

								// Set limits
								ResourceRequirements resources = container.getResources();
								Map<String, Quantity> limits = resources.getLimits();
								if(limits.get(CPU) != null && limits.get(MEMORY) != null) {
									historicalMetricsEntity.setCpuAllocated(limits.get(CPU).getNumericalAmount());
									historicalMetricsEntity.setMemoryAllocated(limits.get(MEMORY).getNumericalAmount());									
								}else {
									throw new GenericException("CPU and/or Memory limits not present in the container [" + container.getName() + "]");
								}

								break;
							}
						}

						if (historicalMetricsEntity.getCpuAllocated() != null && historicalMetricsEntity.getCpuUsed() != null
								&& historicalMetricsEntity.getMemoryAllocated() != null
								&& historicalMetricsEntity.getMemoryUsed() != null
								&& historicalMetricsEntity.getStorageAllocated() != null
								&& historicalMetricsEntity.getStorageUsed() != null) {

							historicalMetricsEntityList.add(historicalMetricsEntity);

						}else {
							log.error("Error retryiving Metrics for container {}:", containerMetrics.getName());
						}
					}
				}
			} else {
				log.error(METRICS_API_IS_NOT_ENABLED_IN_THE_CLUSTER);
			}
		} else {
			log.error(THE_CONNECTION_TO_THE_CLUSTER_IS_NOT_AVAILABLE);
		}

		return historicalMetricsEntityList;
	}

	@Override
	public BigDecimal getAllocatableResources(String resourceType) {

		BigDecimal totalAllocatableResource = Constants.ZERO;

		if(STORAGE.equals(resourceType)) {
			return allocatableStorage;	
		}

		if(kubernetesClient != null) {

			if (kubernetesClient.supports(NodeMetrics.class)) {

				NodeList nodeList = kubernetesClient.nodes().list();

				for (Node node : nodeList.getItems()) {

					BigDecimal nodeAllocatableResource = node.getStatus().getAllocatable().get(resourceType).getNumericalAmount();

					totalAllocatableResource = totalAllocatableResource.add(nodeAllocatableResource);
				}

			} else {
				log.error(METRICS_API_IS_NOT_ENABLED_IN_THE_CLUSTER);
			}
		} else {
			log.error(THE_CONNECTION_TO_THE_CLUSTER_IS_NOT_AVAILABLE);
		}

		return totalAllocatableResource;
	}

	@Override
	public List<CurrentMetricsEntity> getCurrentMetrics() {

		List<CurrentMetricsEntity> currentMetricsEntityList = new ArrayList<>();

		for(HistoricalMetricsEntity historicalMetricsEntity : this.getHistoricalMetrics()) {

			CurrentMetricsEntity currentMetricsEntity = new CurrentMetricsEntity();
			BeanUtils.copyProperties(historicalMetricsEntity, currentMetricsEntity);

			currentMetricsEntityList.add(currentMetricsEntity);
		}

		return currentMetricsEntityList;
	}

}
