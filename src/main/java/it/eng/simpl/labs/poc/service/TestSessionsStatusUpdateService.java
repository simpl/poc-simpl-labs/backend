package it.eng.simpl.labs.poc.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.eng.simpl.labs.poc.entity.TestCaseExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestSuiteExecutionEntity;
import it.eng.simpl.labs.poc.enumeration.TestStatusEnum;
import it.eng.simpl.labs.poc.model.itb.SessionStatus;
import it.eng.simpl.labs.poc.model.itb.SessionStatusResponse;
import it.eng.simpl.labs.poc.repository.TestCaseExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestSuiteExecutionRepository;
import it.eng.simpl.labs.poc.service.interf.IItbTestSessionsManagementService;
import it.eng.simpl.labs.poc.service.interf.ITestSessionsStatusUpdateService;
import it.eng.simpl.labs.poc.utils.GenericUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class TestSessionsStatusUpdateService implements ITestSessionsStatusUpdateService{

	private static final String LOG_PREFIX = "ITB system polling JOB - ";

	private final IItbTestSessionsManagementService itbTestSessionsManagementService;

	private final TestCaseExecutionRepository testCaseExecutionRepository;

	private final TestSuiteExecutionRepository testSuiteExecutionRepository;

	private final TestExecutionRepository testExecutionRepository;

	@Transactional
	@Override
	public void updateTestExecutionStatus() {

		// Get all test cases with endDate null
		List<TestCaseExecutionEntity> testCaseExecutionEntityList = testCaseExecutionRepository.findByEndDateIsNull();

		if(testCaseExecutionEntityList != null && !testCaseExecutionEntityList.isEmpty()) {

			List<String> sessionsList = testCaseExecutionEntityList.stream().map(TestCaseExecutionEntity::getSession).toList();

			log.info(LOG_PREFIX + "Get status for test sessions: {}", sessionsList);
			SessionStatusResponse sessionStatusResponse = itbTestSessionsManagementService.getSessionStatus(sessionsList, Boolean.TRUE, Boolean.TRUE);

			// Test Case Execution status update
			updateTestCaseExecution(testCaseExecutionEntityList, sessionStatusResponse);
			testCaseExecutionRepository.saveAllAndFlush(testCaseExecutionEntityList);
			log.info(LOG_PREFIX + "Updated status for test cases execution: {}", testCaseExecutionEntityList);

			// Test Suite Execution status update
			List<TestSuiteExecutionEntity> testSuiteExecutionEntityList = updateTestSuiteExecution(testCaseExecutionEntityList);
			testSuiteExecutionRepository.saveAllAndFlush(testSuiteExecutionEntityList);
			log.info(LOG_PREFIX + "Updated status for test suites execution: {}", testSuiteExecutionEntityList);

			// Test Execution status update
			TestExecutionEntity testExecutionEntity = updateTestExecution(testSuiteExecutionEntityList);
			testExecutionRepository.saveAndFlush(testExecutionEntity);
			log.info(LOG_PREFIX + "Updated status for test execution: {}", testExecutionEntity);

		} else {
			log.info(LOG_PREFIX + "There are no tests sessions running");
		}
	}

	/**
	 * @param testCaseExecutionEntityList
	 * @param sessionStatusResponse
	 */
	private void updateTestCaseExecution(List<TestCaseExecutionEntity> testCaseExecutionEntityList, SessionStatusResponse sessionStatusResponse) {

		for(TestCaseExecutionEntity testCaseExecutionEntity : testCaseExecutionEntityList) {
			
			boolean sessionFound = false;
			
			for(SessionStatus sessionStatus : sessionStatusResponse.getSessions()) {
				if(testCaseExecutionEntity.getSession().equals(sessionStatus.getSession())) {
					sessionFound = true;
					if(testCaseExecutionEntity.getStartDate() == null) {
						testCaseExecutionEntity.setStartDate(GenericUtils.getLocalDateTimeFromString(sessionStatus.getStartTime()));
					}
					if(testCaseExecutionEntity.getEndDate() == null) {
						testCaseExecutionEntity.setEndDate(GenericUtils.getLocalDateTimeFromString(sessionStatus.getEndTime()));
					}
					testCaseExecutionEntity.setStatus(TestStatusEnum.valueOf(sessionStatus.getResult().getValue()));
					testCaseExecutionEntity.setLogs(sessionStatus.getLogs());
					testCaseExecutionEntity.setReport(sessionStatus.getReport());
				}
			}
			
			if(!sessionFound) {
				// In case the session information is not provided by ITB, it is assumed that the test case execution has been stopped
				TestExecutionEntity testExecutionEntity = testExecutionRepository.findById(testCaseExecutionEntity.getTestSuiteExecution().getTestExecution().getId()).orElse(null);
				if (testExecutionEntity != null && testExecutionEntity.getStopRequestDate() != null) {
					testCaseExecutionEntity.setEndDate(LocalDateTime.now());
					testCaseExecutionEntity.setStatus(TestStatusEnum.UNDEFINED);
				}
			}
		}
	}

	/**
	 * @param testCaseExecutionEntityList
	 * @return
	 */
	private List<TestSuiteExecutionEntity> updateTestSuiteExecution(List<TestCaseExecutionEntity> testCaseExecutionEntityList ) {

		List<TestSuiteExecutionEntity> testSuiteExecutionEntityList = new ArrayList<>();

		if(testCaseExecutionEntityList != null && !testCaseExecutionEntityList.isEmpty()) {

			testSuiteExecutionEntityList = new ArrayList<>(new HashSet<>(testCaseExecutionEntityList.stream().map(TestCaseExecutionEntity::getTestSuiteExecution).toList()));

			for(TestSuiteExecutionEntity testSuiteExecutionEntity : testSuiteExecutionEntityList ) {

				boolean undefined = false;
				boolean failure = false;
				boolean success = false;
				boolean testCaseExecutionCompleted = true;
				
				LocalDateTime startDate = null;
				LocalDateTime endDate = null;

				for(TestCaseExecutionEntity testCaseExecutionEntity : testSuiteExecutionEntity.getTestCaseExecutionList()) {

					if(testCaseExecutionEntity.getEndDate() == null) {
						testCaseExecutionCompleted = false;
					}
					if(endDate == null || (testCaseExecutionEntity.getEndDate() != null && endDate.isBefore(testCaseExecutionEntity.getEndDate()))) {
						endDate = testCaseExecutionEntity.getEndDate();
					}
					if(startDate == null || (testCaseExecutionEntity.getStartDate() != null && startDate.isAfter(testCaseExecutionEntity.getStartDate()))) {
						startDate = testCaseExecutionEntity.getStartDate();
					}
					
					if(TestStatusEnum.UNDEFINED.equals(testCaseExecutionEntity.getStatus())) {					
						undefined = true;
						break;
					}	
					if(TestStatusEnum.FAILURE.equals(testCaseExecutionEntity.getStatus())) {					
						failure = true;
					}
					if(TestStatusEnum.SUCCESS.equals(testCaseExecutionEntity.getStatus())) {					
						success = true;
					}
					
				}

				if(undefined) {
					testSuiteExecutionEntity.setStatus(TestStatusEnum.UNDEFINED);
				} else if(failure) {
					testSuiteExecutionEntity.setStatus(TestStatusEnum.FAILURE);
				} else if(success) {
					testSuiteExecutionEntity.setStatus(TestStatusEnum.SUCCESS);
				} 
				
				if(testSuiteExecutionEntity.getStartDate() == null && startDate != null) {
					testSuiteExecutionEntity.setStartDate(startDate);
				}
				if(testCaseExecutionCompleted && testSuiteExecutionEntity.getEndDate() == null && endDate != null) {
					testSuiteExecutionEntity.setEndDate(endDate);
				}
				
			}
		}

		return testSuiteExecutionEntityList;
	}

	/**
	 * @param testSuiteExecutionEntitylist
	 * @return
	 */
	private TestExecutionEntity updateTestExecution(List<TestSuiteExecutionEntity> testSuiteExecutionEntitylist) {

		TestExecutionEntity testExecutionEntity = null;

		if(testSuiteExecutionEntitylist != null && !testSuiteExecutionEntitylist.isEmpty()){

			boolean undefined = false;
			boolean failure = false;
			boolean success = false;
			boolean testSuiteExecutionCompleted = true;

			LocalDateTime startDate = null;
			LocalDateTime endDate = null;

			testExecutionEntity = testSuiteExecutionEntitylist.get(0).getTestExecution(); 

			for(TestSuiteExecutionEntity testSuiteExecutionEntity : testSuiteExecutionEntitylist) {	
				
				if(testSuiteExecutionEntity.getEndDate() == null) {
					testSuiteExecutionCompleted = false;
				}
				if(endDate == null || (testSuiteExecutionEntity.getEndDate() != null && endDate.isBefore(testSuiteExecutionEntity.getEndDate()))) {
					endDate = testSuiteExecutionEntity.getEndDate();
				}
				if(startDate == null || (testSuiteExecutionEntity.getStartDate() != null && startDate.isAfter(testSuiteExecutionEntity.getStartDate()))) {
					startDate = testSuiteExecutionEntity.getStartDate();
				}
				
				if(TestStatusEnum.UNDEFINED.equals(testSuiteExecutionEntity.getStatus())) {
					undefined = true;
					break;
				}
				if(TestStatusEnum.FAILURE.equals(testSuiteExecutionEntity.getStatus())) {
					failure = true;
				}
				if(TestStatusEnum.SUCCESS.equals(testSuiteExecutionEntity.getStatus())) {
					success = true;
				}
				
			}

			if(undefined) {
				testExecutionEntity.setStatus(TestStatusEnum.UNDEFINED);
			} else if(failure) {
				testExecutionEntity.setStatus(TestStatusEnum.FAILURE);
			} else if(success) {
				testExecutionEntity.setStatus(TestStatusEnum.SUCCESS);
			} 
			
			if(testExecutionEntity.getStartDate() == null && startDate != null) {
				testExecutionEntity.setStartDate(startDate);
			}
			if(testSuiteExecutionCompleted && testExecutionEntity.getEndDate() == null && endDate != null) {
				testExecutionEntity.setEndDate(endDate);
			}
		}

		return testExecutionEntity;
	}
	
}
