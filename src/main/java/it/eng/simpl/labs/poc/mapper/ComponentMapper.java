package it.eng.simpl.labs.poc.mapper;

import it.eng.simpl.labs.poc.entity.ComponentEntity;
import it.eng.simpl.labs.poc.model.Component;

@org.springframework.stereotype.Component
public class ComponentMapper extends BaseMapper<Component, ComponentEntity> {

	@Override
	public ComponentEntity createFromDto(Component dto) {

		ComponentEntity entity = new ComponentEntity();
		
		copyProperties(dto, entity);
		
		return entity;
	}

	@Override
	public Component createFromEntity(ComponentEntity entity) {

		Component dto = Component.builder().build();

		copyProperties(entity, dto);

		return dto;
	}

}
