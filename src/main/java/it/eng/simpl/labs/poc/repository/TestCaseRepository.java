package it.eng.simpl.labs.poc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.eng.simpl.labs.poc.entity.TestCaseEntity;
import it.eng.simpl.labs.poc.entity.TestCaseEntityPK;

@Repository
public interface TestCaseRepository extends BaseRepository<TestCaseEntity, TestCaseEntityPK> {

	public List<TestCaseEntity> findByTestSuiteId(String testSuiteId);
	
	public List<TestCaseEntity> findByTestSuiteIdIn(List<String> testSuiteIdList);
	
	public List<TestCaseEntity> findByIdTestCaseId(String testCaseId);
	
	public List<TestCaseEntity> findByIdTestCaseIdIn(List<String> testCaseIdList);

	@Query("SELECT COUNT(a) FROM TestCaseEntity a,TestSuiteEntity b WHERE a.testSuite.id=b.id and b.specificationId = ?1")
	public Integer countTotals(Long specificationId);

	
}
