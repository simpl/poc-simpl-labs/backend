package it.eng.simpl.labs.poc.enumeration;

public enum TestStatusEnum {
	
	SUCCESS("Success"),
	FAILURE("Failure"),
	ONGOING("Ongoing"),
	UNDEFINED("Undefined");
	
	private String value;

	private TestStatusEnum(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
