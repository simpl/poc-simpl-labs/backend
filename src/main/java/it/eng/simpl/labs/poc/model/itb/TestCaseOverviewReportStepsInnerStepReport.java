package it.eng.simpl.labs.poc.model.itb;

import it.eng.simpl.labs.poc.enumeration.ItbStepResultEnum;
import lombok.Data;

@Data
public class TestCaseOverviewReportStepsInnerStepReport {

	private String id;

	private String date;

	private ItbStepResultEnum result;

	private TestCaseOverviewReportStepsInnerStepReportReports reports;

}
