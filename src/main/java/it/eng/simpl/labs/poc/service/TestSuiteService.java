package it.eng.simpl.labs.poc.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.eng.simpl.labs.poc.entity.TestCaseEntity;
import it.eng.simpl.labs.poc.entity.TestCaseExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestSuiteEntity;
import it.eng.simpl.labs.poc.enumeration.TestStatusEnum;
import it.eng.simpl.labs.poc.mapper.TestSuiteMapper;
import it.eng.simpl.labs.poc.model.TestSuite;
import it.eng.simpl.labs.poc.repository.TestCaseExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestCaseRepository;
import it.eng.simpl.labs.poc.repository.TestSuiteRepository;
import it.eng.simpl.labs.poc.service.interf.ITestSuiteService;
import it.eng.simpl.labs.poc.utils.GenericUtils;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class TestSuiteService implements ITestSuiteService{

	private final TestSuiteRepository testSuiteRepository;

	private final TestSuiteMapper testSuiteMapper;

	private final TestCaseRepository testCaseRepository;

	private final TestCaseExecutionRepository testCaseExecutionRepository;

	@Override
	public List<TestSuite> findBySpecificationId(Long specificationId, Long componentId) {

		List<TestSuite> testSuiteList = new ArrayList<>();

		// Get all test suites associated to the provided specification

		List<TestSuiteEntity> testSuiteEntityList = testSuiteRepository.findBySpecificationId(specificationId);

		for (TestSuiteEntity testSuiteEntity : testSuiteEntityList) {

			TestSuite testSuite = testSuiteMapper.createFromEntity(testSuiteEntity);

			testSuite.setLastUpdate(testSuiteEntity.getLastUpdateDate());

			// Set test cases number

			testSuite.setTestCasesNumber(testSuiteEntity.getTestCasesList() != null ? testSuiteEntity.getTestCasesList().size() : 0);

			Integer totalTestsNumber = testSuite.getTestCasesNumber();
			Integer notExecutedTestsNumber = 0;
			Integer errorTestsNumber = 0;
			Integer successTestNumber = 0;

			// Get all test cases associated to the provided test suite
			List<TestCaseEntity> testCaseEntityList = testCaseRepository.findByTestSuiteId(testSuiteEntity.getId());

			for(TestCaseEntity testCaseEntity : testCaseEntityList) {

				//For every test case we must know if it was included in a previous execution for this component and, if included, the result
				List<TestCaseExecutionEntity> testCaseExecutionList=testCaseExecutionRepository.findByTestCaseExecution(
						testCaseEntity.getId().getTestCaseId(),
						testCaseEntity.getId().getTestSuiteId(),
						componentId,
						PageRequest.of(0,1));

				if (testCaseExecutionList!=null && !testCaseExecutionList.isEmpty()) {
					if(TestStatusEnum.SUCCESS.equals(testCaseExecutionList.get(0).getStatus())) {
						successTestNumber++;
					}
					if(TestStatusEnum.FAILURE.equals(testCaseExecutionList.get(0).getStatus())) {
						errorTestsNumber++;
					}
				}
			}

			notExecutedTestsNumber = totalTestsNumber - (errorTestsNumber + successTestNumber);
			testSuite.setResultError(GenericUtils.getPercentage(errorTestsNumber, totalTestsNumber));
			testSuite.setResultSuccess(GenericUtils.getPercentage(successTestNumber, totalTestsNumber));
			testSuite.setResultNotExecuted(GenericUtils.getPercentage(notExecutedTestsNumber, totalTestsNumber));

			testSuiteList.add(testSuite);

		}

		return testSuiteList;
	}

}
