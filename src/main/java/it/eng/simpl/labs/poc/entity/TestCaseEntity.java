package it.eng.simpl.labs.poc.entity;

import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class TestCaseEntity extends BaseEntity {
	
    @EmbeddedId
    private TestCaseEntityPK id;
		
	@Column(nullable = false)
	private String name;
	
	@Column
	private String description;
	
	@Column
	private String version;
	
	@JdbcTypeCode(SqlTypes.JSON)
    @Column(name = "steps", columnDefinition = "jsonb")
	private String steps;
	
    @MapsId("testSuiteId")
    @ManyToOne
    @JoinColumn(name = "test_suite_id", referencedColumnName = "id", insertable = false, updatable = false)
	private TestSuiteEntity testSuite;
    
}
