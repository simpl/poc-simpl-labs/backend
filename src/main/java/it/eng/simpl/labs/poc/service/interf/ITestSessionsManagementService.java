package it.eng.simpl.labs.poc.service.interf;

import it.eng.simpl.labs.poc.model.TestSessionStartRequest;
import it.eng.simpl.labs.poc.model.TestSessionStartResponse;
import it.eng.simpl.labs.poc.model.itb.SessionStopRequest;
import it.eng.simpl.labs.poc.model.itb.SessionStopResponse;

public interface ITestSessionsManagementService {

	/**
	 * @param localSessionStartRequest
	 * @return
	 */
	public TestSessionStartResponse startTestSession(TestSessionStartRequest localSessionStartRequest);
	
	/**
	 * Stop test session
	 * 
	 * @param sessionStopRequest
	 * @return
	 */
	public SessionStopResponse stopTestSession(SessionStopRequest sessionStopRequest);

}
