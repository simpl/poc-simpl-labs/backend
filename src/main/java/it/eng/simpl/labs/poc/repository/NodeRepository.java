package it.eng.simpl.labs.poc.repository;

import org.springframework.stereotype.Repository;

import it.eng.simpl.labs.poc.entity.NodeEntity;

@Repository
public interface NodeRepository extends BaseRepository<NodeEntity, Long>{
}