package it.eng.simpl.labs.poc.entity;

import java.time.LocalDateTime;

import it.eng.simpl.labs.poc.enumeration.TestStatusEnum;
import jakarta.persistence.Column;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public class BaseTestExecutionEntity extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	@Min(0)
	@Max(9223372036854775807l)
	protected Long id;
	
	@Enumerated
	protected TestStatusEnum status;
	
	@Column
	protected LocalDateTime startDate;
	
	@Column
	protected LocalDateTime endDate;

}
