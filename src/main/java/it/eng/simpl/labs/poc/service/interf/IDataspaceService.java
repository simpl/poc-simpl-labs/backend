package it.eng.simpl.labs.poc.service.interf;

import java.util.List;

import it.eng.simpl.labs.poc.model.Dataspace;
import it.eng.simpl.labs.poc.model.User;

public interface IDataspaceService {
	
	/**
	 * @param id
	 * @return
	 */
	public Dataspace findById(Long id);
	
	/**
	 * Returns all dataspaces for a user
	 * 
	 * @param user
	 * @return
	 */
	public List<Dataspace> findAll(User user);
	
	/**
	 * Returns all dataspaces
	 * 
	 * @param user
	 * @return
	 */
	public List<Dataspace> findAll();
	
	/**
	 * Request the creation of a dataspace
	 * 
	 * @param dataspace
	 * @param user
	 * @param debug
	 * @return
	 */
	public Dataspace save(Dataspace dataspace, User user);
	
	/**
	 * Request the deletion of a dataspace 
	 * 
	 * @param dataspaceId
	 * @param user
	 * @return
	 */
	public Dataspace delete(Long dataspaceId, User user);

}
