package it.eng.simpl.labs.poc.service.interf;

import java.time.LocalDateTime;
import java.util.List;

import it.eng.simpl.labs.poc.entity.CurrentMetricsEntity;

public interface IMetricsManagementService {
		
	/**
	 * @param date
	 */
	public void deleteByCreateDateLessThan(LocalDateTime date);
	
	/**
	 * 
	 */
	public void deleteOrphans();
		
	/**
	 * Get Metrics from Kuernetes cluster and save them on local data base
	 */
	public void updateMetrics();
	
	/**
	 * Delete from local data base all metrics older than @date
	 */
	public void deleteOldMetrics(LocalDateTime date);
		
	/**
	 * Get Metrics from Kuernetes cluster and save them on local buffer data base
	 */
	public List<CurrentMetricsEntity> updateBufferedMetrics();

}
