package it.eng.simpl.labs.poc.model;

import java.time.LocalDateTime;
import java.util.List;

import it.eng.simpl.labs.poc.enumeration.DataspaceStatusEnum;
import it.eng.simpl.labs.poc.enumeration.DataspaceTypeEnum;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class Dataspace {
	private Long id;
	private List<Node> nodeList;
	private String name;
	private String description;
	private List<String> tagList;
	private DataspaceStatusEnum status;
	private DataspaceTypeEnum type;
	private User user;
	private LocalDateTime createDate;
}
