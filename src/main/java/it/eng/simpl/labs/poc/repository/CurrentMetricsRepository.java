package it.eng.simpl.labs.poc.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.eng.simpl.labs.poc.entity.CurrentMetricsEntity;

@Repository
public interface CurrentMetricsRepository extends BaseRepository<CurrentMetricsEntity, UUID> {
    		
	@Query(	"SELECT c FROM CurrentMetricsEntity c INNER JOIN DataspaceEntity d ON (c.dataspaceId = d.id) WHERE d.userId = ?1 ORDER BY d.id ASC")
	public List<CurrentMetricsEntity> getGeneralMetrics(String user);
	
	public Page<CurrentMetricsEntity> findByDataspaceId(Long dataspaceId, Pageable pageable);
	
	public List<CurrentMetricsEntity> findByDataspaceId(Long dataspaceId);
	
	public Page<CurrentMetricsEntity> findByNodeId(Long nodeId, Pageable pageable);
	
	public List<CurrentMetricsEntity> findByNodeId(Long nodeId);
	
	public Page<CurrentMetricsEntity> findByCategoryId(Long categoryId, Pageable pageable);
	
	public List<CurrentMetricsEntity> findByCategoryId(Long categoryId);
	
	public Page<CurrentMetricsEntity> findByComponentId(Long componentId, Pageable pageable);
	
	public List<CurrentMetricsEntity> findByComponentId(Long componentId);
	
	@Modifying
	@Query(value ="DELETE FROM CurrentMetricsEntity")
	public void deleteAllMetrics();

}