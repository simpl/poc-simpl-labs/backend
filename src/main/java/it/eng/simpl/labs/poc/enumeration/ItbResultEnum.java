package it.eng.simpl.labs.poc.enumeration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ItbResultEnum {
	
	SUCCESS("SUCCESS"),

	FAILURE("FAILURE"),

	UNDEFINED("UNDEFINED");

	private String value;

	ItbResultEnum(String value) {
		this.value = value;
	}

	@JsonValue
	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	@JsonCreator
	public static ItbResultEnum fromValue(String value) {
		for (ItbResultEnum b : ItbResultEnum.values()) {
			if (b.value.equals(value)) {
				return b;
			}
		}
		throw new IllegalArgumentException("Unexpected value '" + value + "'");
	}
}
