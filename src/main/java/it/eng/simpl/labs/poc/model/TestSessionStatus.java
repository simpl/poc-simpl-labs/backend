package it.eng.simpl.labs.poc.model;

import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TestSessionStatus {

	@Schema(name = "testExecution", description = "The test session general information")
	private TestExecution testExecution;

	@Schema(name = "testCasesList", description = "The session test cases list")
	private List<TestCase> testCasesList;

}
