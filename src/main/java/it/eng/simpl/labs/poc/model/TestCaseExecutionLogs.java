package it.eng.simpl.labs.poc.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TestCaseExecutionLogs {

	@Schema(name = "logs", description = "The test case execution logs")
	private String logs;

}
