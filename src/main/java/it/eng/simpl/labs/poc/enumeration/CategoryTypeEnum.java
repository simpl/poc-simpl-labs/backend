package it.eng.simpl.labs.poc.enumeration;

import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import lombok.ToString;

@ToString
public enum CategoryTypeEnum {

	SD_TOOLING("SD_TOOLING", "sdTooling"), 
	DATA_SERVICES("DATA_SERVICES", "dataServices"),
	APPLICATION_SERVICES("APPLICATION_SERVICES", "applicationServices"),
	INFRASTRUCTURE_SERVICES("INFRASTRUCTURE_SERVICES", "infrastructureServices"), 
	IAA_LOCAL("IAA_LOCAL", "iaaLocal"),
	LOGGING("LOGGING", "logging"), 
	CONSENT("CONSENT", "consent"), 
	MONITORING("MONITORING", "monitoring"),
	REPORTING("REPORTING", "reporting"), 
	CONTRACT("CONTRACT", "contract"), 
	DISCOVERY("DISCOVERY", "discovery"),
	IDP("IDP", "idp"), 
	VOCABULARY("VOCABULARY", "vocabulary"), 
	REGISTRY("REGISTRY", "registry"),
	SUPPORT("SUPPORT", "support"),
	APPLICATION_SHARING("APPLICATION_SHARING", "applicationSharing"), 
	DATA_SHARING("DATA_SHARING", "dataSharing"); 

	private String value;
	private String prefix;

	private CategoryTypeEnum(String value, String prefix) {
		this.value = value;
		this.prefix = prefix;
	}

	@JsonCreator
	public static CategoryTypeEnum decode(final String value) {
		return Stream.of(CategoryTypeEnum.values()).filter(targetEnum -> targetEnum.getValue().equals(value))
				.findFirst().orElse(null);
	}

	@JsonValue
	public String getValue() {
		return value;
	}

	public String getPrefix() {
		return prefix;
	}

}
