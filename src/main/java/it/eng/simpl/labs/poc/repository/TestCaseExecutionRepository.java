package it.eng.simpl.labs.poc.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import it.eng.simpl.labs.poc.entity.TestCaseExecutionEntity;

@Repository
public interface TestCaseExecutionRepository extends BaseRepository<TestCaseExecutionEntity, Long> {
	
	@Query("SELECT tc FROM TestCaseExecutionEntity tc, TestSuiteExecutionEntity ts, TestExecutionEntity te, CustomComponentEntity cc "
			+ "WHERE tc.testSuiteExecutionId = ts.id "
			+ "and ts.testExecutionId = te.id "
			+ "and te.customComponentId = cc.id "
			+ "and tc.testCaseId = ?1 "
			+ "and cc.id = ?2 "
			+ "and tc.status != 2 "
			+ "and tc.status != 3 "
			+ "order by tc.lastUpdateDate desc")
	public List<TestCaseExecutionEntity> findLastExecution(String testCaseId, Long componentId);

	public List<TestCaseExecutionEntity> findByStatusIsNullOrStatusNotIn(List<Integer> statusList);
	
	public List<TestCaseExecutionEntity> findByEndDateIsNull();
	
	public TestCaseExecutionEntity findBySession(String testSessionId);
	
	public List<TestCaseExecutionEntity> findBySessionIn(List<String> testSessionsList);
	
	public List<TestCaseExecutionEntity> findByTestCaseIdAndTestSuiteIdOrderByLastUpdateDateDesc(String testCaseId, String testSuiteId);
	
	@Query("SELECT a FROM TestCaseExecutionEntity a,TestSuiteExecutionEntity b, TestExecutionEntity c "
			+ "WHERE a.testCaseId = ?1 and a.testSuiteId = ?2 "
			+ "and a.testSuiteExecution.id=b.id "
			+ "and b.testExecutionId=c.id "
			+ "and c.customComponentId = ?3 "
			+ "and c.status != 2 "
			+ "and c.status != 3 "
			+ "order by a.lastUpdateDate desc")
	public List<TestCaseExecutionEntity> findByTestCaseExecution(String testCaseId, String testSuiteId, Long customComponentId, Pageable pageable);

}
