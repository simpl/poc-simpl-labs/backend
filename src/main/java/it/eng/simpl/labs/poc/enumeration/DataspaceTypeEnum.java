package it.eng.simpl.labs.poc.enumeration;

public enum DataspaceTypeEnum {
	EXPERIMENTATION, 
	COMPLIANCE_TESTING, 
	COLLABORATION, 
	PRODUCTION_SIMULATION;
}
