package it.eng.simpl.labs.poc.service.interf;

import it.eng.simpl.labs.poc.model.Dataspace;

public interface IDeploymentService {

	public final String FE = "Fe";
	public final String BE = "Be";
	public final String PARAM6 = ".param6";
	public final String PARAM5 = ".param5";
	public final String PARAM4 = ".param4";
	public final String PARAM3 = ".param3";
	public final String PARAM2 = ".param2";
	public final String PARAM1 = ".param1";
	public final String CONSUMER = "-consumer-";
	public final String INFRA = "-infra-";
	public final String DATA = "-data-";
	public final String APP = "-app-";
	public final String GA = "-ga-";
	public final String DATASPACE_PREFIX = "dataspace-";
	
	/**
	 * @param dataspace
	 */
	public void initDataspaceCreation(Dataspace dataspace);
	
	/**
	 * Asynchronous deletion of a data space
	 * @param dataspaceToDelete
	 */
	public void initDataspaceDeletion(Dataspace dataspaceToDelete);
}
