package it.eng.simpl.labs.poc.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TestSuite {
	
	@Schema(name = "id", description = "The test suite id")
	private String id;
		
	@Schema(name = "name", description = "The test suite name")
	private String name;
	
	@Schema(name = "description", description = "The test suite description")
	private String description;
	
	@Schema(name = "version", description = "The test suite version")
	private String version;
	
	@Schema(name = "lastUpdate", description = "The test suite last update time")
	private LocalDateTime lastUpdate;

	@Schema(name = "testCasesNumber", description = "The test suite test cases number")
	private Integer testCasesNumber;
	
	@Schema(name = "resultSuccess", description = "The percentage of successfully executed tests")
	private BigDecimal resultSuccess;
	
	@Schema(name = "resultError", description = "The percentage of test executed with error")
	private BigDecimal resultError;
	
	@Schema(name = "resultNotExecuted", description = "The percentage of not executed tests")
	private BigDecimal resultNotExecuted;
	
	@JsonIgnore
	private List<TestCase> testCasesList;
	
}
