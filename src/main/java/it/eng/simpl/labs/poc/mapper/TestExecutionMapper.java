package it.eng.simpl.labs.poc.mapper;

import org.springframework.stereotype.Component;

import it.eng.simpl.labs.poc.entity.TestCaseExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestSuiteExecutionEntity;
import it.eng.simpl.labs.poc.enumeration.TestStatusEnum;
import it.eng.simpl.labs.poc.model.TestExecution;

@Component
public class TestExecutionMapper extends BaseMapper<TestExecution, TestExecutionEntity> {

	@Override
	public TestExecutionEntity createFromDto(TestExecution dto) {
        throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public TestExecution createFromEntity(TestExecutionEntity entity) {

		TestExecution dto = null;

		if (entity != null) {
			
			dto = TestExecution.builder().build();
			
			copyProperties(entity, dto);
			
			// Status mapping
			dto.setStatus(TestStatusEnum.UNDEFINED.getValue());
			if(entity.getStatus() != null) {
				if(TestStatusEnum.UNDEFINED.equals(entity.getStatus()) && entity.getEndDate() == null) {
					dto.setStatus(TestStatusEnum.ONGOING.getValue());
				}else {
					dto.setStatus(entity.getStatus().getValue());
				}
			}
			
			dto.setComponentId(entity.getCustomComponent().getId()+"");
			dto.setComponentName(entity.getCustomComponent().getName());
			dto.setSpecificationId(entity.getCustomComponent().getSpecification().getId());
			dto.setSpecificationName(entity.getCustomComponent().getSpecification().getName());
			dto.setSpecificationLastUpdate(entity.getCustomComponent().getSpecification().getLastUpdateDate());
			dto.setStartDate(entity.getStartDate());
			dto.setEndDate(entity.getEndDate());
			
			Long totalTestsNumber = 0L;
			Long passedTestsNumber = 0L;
			for(TestSuiteExecutionEntity testSuiteExecutionEntity : entity.getTestSuiteExecutionList()) {
				for(TestCaseExecutionEntity testCaseExecutionEntity : testSuiteExecutionEntity.getTestCaseExecutionList()) {
					totalTestsNumber++;
					if(TestStatusEnum.SUCCESS.equals(testCaseExecutionEntity.getStatus())) {
						passedTestsNumber++;
					}
				}
			}
			
			dto.setTotalTestsNumber(totalTestsNumber);
			dto.setPassedTestsNumber(passedTestsNumber);
		}

		return dto;
	}

}
