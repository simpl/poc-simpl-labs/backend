package it.eng.simpl.labs.poc.utils;

import org.springframework.security.oauth2.jwt.Jwt;

import it.eng.simpl.labs.poc.model.JwtUser;
import it.eng.simpl.labs.poc.model.User;

public class JwtUtil {
	
	private JwtUtil() {
		throw new IllegalStateException("Utility class");
	}
	
	private static final String NAME = "name";
	private static final String PREFERRED_USERNAME = "preferred_username";
	private static final String EMAIL = "email";

	public static JwtUser createJwtUser(Jwt jwt) {

		User user = User.builder().email(jwt.getClaimAsString(EMAIL)).username(jwt.getClaimAsString(PREFERRED_USERNAME))
				.name(jwt.getClaimAsString(NAME)).build();
		return new JwtUser(jwt, null, user);
	}
}
