package it.eng.simpl.labs.poc.model.itb;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class TestCaseOverviewReportStepsInnerStepReportReports {
	
  private List<TestCaseOverviewReportStepsInnerStepReportReportsErrorInner> error = new ArrayList<>();

  private List<TestCaseOverviewReportStepsInnerStepReportReportsErrorInner> warning = new ArrayList<>();

  private List<TestCaseOverviewReportStepsInnerStepReportReportsErrorInner> info = new ArrayList<>();

}

