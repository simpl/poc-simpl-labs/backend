package it.eng.simpl.labs.poc.entity;

import java.util.List;

import it.eng.simpl.labs.poc.enumeration.DataspaceStatusEnum;
import it.eng.simpl.labs.poc.enumeration.DataspaceTypeEnum;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class DataspaceEntity extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	@Min(0)
	@Max(9223372036854775807l)
	private Long id;
	
	@Column
	private String name;
	
	@Column
	private String description;
	
	@Column
	@ElementCollection
	private List<String> tagList;
	
	@Column
	@Enumerated
	private DataspaceStatusEnum status;
	
	@Column
	@Enumerated
	private DataspaceTypeEnum type;
	
	@OneToMany(mappedBy = "dataspace", cascade = CascadeType.ALL)
	private List<NodeEntity> nodeList;
}
