package it.eng.simpl.labs.poc.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import it.eng.simpl.labs.poc.model.CurrentConsumption;
import it.eng.simpl.labs.poc.model.CurrentConsumptionWrapper;
import it.eng.simpl.labs.poc.model.GenericResponse;
import it.eng.simpl.labs.poc.model.HistoricalConsumption;
import it.eng.simpl.labs.poc.model.User;
import it.eng.simpl.labs.poc.service.interf.IComponentsMetricsService;
import it.eng.simpl.labs.poc.utils.GenericUtils;
import lombok.RequiredArgsConstructor;

@Tag(name = "System Monitoring", description = "System Monitoring management APIs")
@RestController
@RequestMapping("/system-monitoring")
@RequiredArgsConstructor
public class ComponentsMonitoringController {
	
	private final IComponentsMetricsService componentsMetricsService;
	
	@GetMapping("/component/current/{componentId}")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the category current resources usage")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = CurrentConsumptionWrapper.class))) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })
	public Page<CurrentConsumption> getDataspacesCurrent(
			@Parameter @PathVariable("componentId")  Long componentId,
			@RequestParam(value = "pageNumber", required = true) Integer pageNumber,
            @RequestParam(value = "pageSize", required = true) Integer pageSize,
            @RequestParam(value = "sortBy", required = false) String sortBy,
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user ) {

		return componentsMetricsService.getCurrentMetrics(componentId, GenericUtils.getPageRequest(pageNumber, pageSize, sortBy));

	}
			
	@GetMapping("/component/historical/cpu/{componentId}/{startDate}/{endDate}")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the component historical CPU usage")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = CurrentConsumptionWrapper.class))) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })
	public List<HistoricalConsumption> getDataspacesHistoricalCpu(
			@Parameter @PathVariable("componentId") Long componentId,
			@Parameter @PathVariable("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
			@Parameter @PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate,
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user ) {

		return componentsMetricsService.getHistoricalCpuMetrics(componentId, startDate, endDate);

	}
	
	@GetMapping("/component/historical/memory/{componentId}/{startDate}/{endDate}")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the component historical Memory usage")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = HistoricalConsumption.class))) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })
	public List<HistoricalConsumption> getDataspacesHistoricalMemory(
			@Parameter @PathVariable("componentId") Long componentId,
			@Parameter @PathVariable("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
			@Parameter @PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate,
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user ) {

		return componentsMetricsService.getHistoricalMemoryMetrics(componentId, startDate, endDate);

	}
	
	@GetMapping("/component/historical/storage/{componentId}/{startDate}/{endDate}")
	@SecurityRequirement(name = "Authentication")
	@Operation(summary = "Get the component historical Storage usage")
	@ApiResponse(responseCode = "200", description = "Action successfully executed", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = HistoricalConsumption.class))) })
	@ApiResponse(responseCode = "500", description = "Error executing action", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GenericResponse.class)) })
	public List<HistoricalConsumption> getDataspacesHistoricalStorage(
			@Parameter @PathVariable("componentId") Long componentId,
			@Parameter @PathVariable("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
			@Parameter @PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate,
			@CurrentSecurityContext(expression = "authentication.user") @Parameter(description = "The current user") User user ) {

		return componentsMetricsService.getHistoricalStorageMetrics(componentId, startDate, endDate);

	}

}
