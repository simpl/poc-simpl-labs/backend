package it.eng.simpl.labs.poc.mapper;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import it.eng.simpl.labs.poc.entity.CustomComponentEntity;
import it.eng.simpl.labs.poc.entity.TestCaseEntity;
import it.eng.simpl.labs.poc.entity.TestCaseExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestSuiteEntity;
import it.eng.simpl.labs.poc.enumeration.TestStatusEnum;
import it.eng.simpl.labs.poc.model.CustomComponent;
import it.eng.simpl.labs.poc.repository.TestCaseExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestCaseRepository;
import it.eng.simpl.labs.poc.repository.TestSuiteRepository;
import it.eng.simpl.labs.poc.utils.GenericUtils;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class CustomComponentMapper extends BaseMapper<CustomComponent, CustomComponentEntity> {

	private final TestSuiteRepository testSuiteRepository;
	
	private final TestCaseRepository testCaseRepository;
	
	private final TestCaseExecutionRepository testCaseExecutionRepository;

	@Override
	public CustomComponentEntity createFromDto(CustomComponent dto) {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public CustomComponent createFromEntity(CustomComponentEntity entity) {

		CustomComponent dto = CustomComponent.builder().build();

		copyProperties(entity, dto);

		dto.setSpecificationDescription(entity.getSpecification().getName());
		dto.setSpecificationId(entity.getSpecification().getId());
		dto.setSpecificationLastUpdate(entity.getSpecification().getLastUpdateDate());
		dto.setCategory(entity.getCategory().getValue());
		dto.setNodeType(entity.getNodeType().name());
		
		// Status mapping
		dto.setStatus(TestStatusEnum.UNDEFINED.getValue());
		if(entity.getTestexecutionList() != null && !entity.getTestexecutionList().isEmpty() &&
				entity.getTestexecutionList().get(entity.getTestexecutionList().size()-1) != null &&
				entity.getTestexecutionList().get(entity.getTestexecutionList().size()-1).getStatus() != null) {
			
			if(TestStatusEnum.UNDEFINED.equals(entity.getTestexecutionList().get(entity.getTestexecutionList().size()-1).getStatus()) &&
					entity.getTestexecutionList().get(entity.getTestexecutionList().size()-1).getEndDate() == null) {
				
				dto.setStatus(TestStatusEnum.ONGOING.getValue());
				
			}else {
				
				dto.setStatus(entity.getTestexecutionList().get(entity.getTestexecutionList().size()-1).getStatus().getValue());
				
			}
		}

		Integer totalTestsNumber = 0;
		Integer notExecutedTestsNumer = 0;
		Integer errorTestsNumber = 0;
		Integer successTestNumber = 0;
		
		totalTestsNumber = testCaseRepository.countTotals(entity.getSpecificationId());
		
		List<TestSuiteEntity> testSuiteEntityList = testSuiteRepository.findBySpecificationId(entity.getSpecification().getId());

		for(TestSuiteEntity testSuiteEntity : testSuiteEntityList) {
			
			// Get all test cases associated to the provided test suite
			List<TestCaseEntity> testCaseEntityList = testCaseRepository.findByTestSuiteId(testSuiteEntity.getId());
			
			for(TestCaseEntity testCaseEntity : testCaseEntityList) {
				
				//For every test case we must know if it was included in a previous execution for this component and, if included, the result
				List<TestCaseExecutionEntity> testCaseExecutionList = testCaseExecutionRepository.findByTestCaseExecution(
						testCaseEntity.getId().getTestCaseId(),
						testCaseEntity.getId().getTestSuiteId(),
						entity.getId(),
						PageRequest.of(0,1));
				
				if (testCaseExecutionList != null && !testCaseExecutionList.isEmpty()) {
					if(TestStatusEnum.SUCCESS.equals(testCaseExecutionList.get(0).getStatus())) {
						successTestNumber++;
					}
					if(TestStatusEnum.FAILURE.equals(testCaseExecutionList.get(0).getStatus())) {
						errorTestsNumber++;
					}
				}
			}
		}
		
		notExecutedTestsNumer = totalTestsNumber - (errorTestsNumber + successTestNumber);
		dto.setResultError(GenericUtils.getPercentage(errorTestsNumber, totalTestsNumber));
		dto.setResultSuccess(GenericUtils.getPercentage(successTestNumber, totalTestsNumber));
		dto.setResultNotExecuted(GenericUtils.getPercentage(notExecutedTestsNumer, totalTestsNumber));
		
		if(!dto.getStatus().equals(TestStatusEnum.ONGOING.getValue())) {
			
			if (dto.getResultError() != null && dto.getResultError().compareTo(BigDecimal.ZERO) > 0) {
				dto.setStatus(TestStatusEnum.FAILURE.getValue());
			} else if (dto.getResultNotExecuted() != null && dto.getResultNotExecuted().compareTo(BigDecimal.ZERO) > 0) {
				dto.setStatus(TestStatusEnum.UNDEFINED.getValue());
			} else if (totalTestsNumber.compareTo(successTestNumber) == 0) {
				dto.setStatus(TestStatusEnum.SUCCESS.getValue());
			}
		}
		
		return dto;
	}

}
