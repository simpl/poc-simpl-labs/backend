package it.eng.simpl.labs.poc.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import it.eng.simpl.labs.poc.entity.TestSuiteExecutionEntity;

@Repository
public interface TestSuiteExecutionRepository extends BaseRepository<TestSuiteExecutionEntity, Long> {

	public List<TestSuiteExecutionEntity> findByTestSuiteIdAndTestExecutionCustomComponentIdOrderByLastUpdateDateDesc(String testSuiteId, Long customComponentId);

	public List<TestSuiteExecutionEntity> findByTestExecutionIdIn(List<Long> testExecutionId);

}
