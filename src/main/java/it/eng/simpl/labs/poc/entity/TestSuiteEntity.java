package it.eng.simpl.labs.poc.entity;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class TestSuiteEntity extends BaseEntity {
	
	@Id
	@Column(name = "id", unique = true, nullable = false)
	private String id;
	
	@Column(nullable = false)
	private String name;
	
	@Column
	private String description;
	
	@Column
	private String version;
	
	@Column(name = "specification_id")
	private Long specificationId;
	
	@ManyToOne
	@JoinColumn(name = "specification_id", insertable = false, updatable = false)
	private SpecificationEntity specification;
	
	@OneToMany(mappedBy = "testSuite")
	private List<TestCaseEntity> testCasesList;
	
}
