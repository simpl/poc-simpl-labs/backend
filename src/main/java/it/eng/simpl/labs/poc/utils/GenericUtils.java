package it.eng.simpl.labs.poc.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import it.eng.simpl.labs.poc.model.HistoricalConsumption;

public class GenericUtils {

	private GenericUtils() {
		throw new IllegalStateException("Utility class");
	}
	
	/**
	 * @param offset
	 * @param pageSize
	 * @param sortBy
	 * @return
	 */
	public static PageRequest getPageRequest(Integer pageNumber, Integer pageSize, String sortBy) {
				
		PageRequest pageRequest = null;
		
		if(sortBy != null ) {
			pageRequest = PageRequest.of(pageNumber != null ? pageNumber : Constants.DEFAULT_PAGINATION_OFFSET, pageSize != null ? pageSize :  Constants.DEFAULT_PAGE_SIZE, Sort.by(sortBy));
		}else {
			pageRequest = PageRequest.of(pageNumber != null ? pageNumber :  Constants.DEFAULT_PAGINATION_OFFSET, pageSize != null ? pageSize : Constants.DEFAULT_PAGE_SIZE);
		}
		
		return pageRequest;
	}
	
	/**
	 * @param list
	 * @param i
	 * @return
	 */
	public static List<HistoricalConsumption> getModelFromObjectsList(List<Object[]> list, int i) {

		List<HistoricalConsumption> historicalConsumpionList = new ArrayList<>();

		for(Object[] objectList : list) {
			HistoricalConsumption historicalNodesConsumpions = HistoricalConsumption.builder()
					.date(((java.sql.Date) objectList[0]).toLocalDate())
					.allocated(new BigDecimal(""+objectList[i]))
					.used(new BigDecimal(""+objectList[i+1]))
					.build();
			historicalConsumpionList.add(historicalNodesConsumpions);
		}

		return historicalConsumpionList;
	}

	/**
	 * Get the percentage of {@code partial} in respect to {@code total}
	 * 
	 * @param partial
	 * @param total
	 * @return the percentage 
	 */
	public static BigDecimal getPercentage(Integer partial, Integer total) {
		
		if(0 == (total == null ? 0 : total)) {
			return null;
		}

		int scale = 2;
		
		BigDecimal bpartial = BigDecimal.valueOf(partial); 
		BigDecimal btotal = BigDecimal.valueOf(total); 
		
        BigDecimal percentage = bpartial.divide(btotal, scale + 2, RoundingMode.HALF_DOWN);
        
        return percentage.setScale(scale, RoundingMode.HALF_DOWN);

	}
	

	/**
	 * Copy the property values of the given source bean into the target bean.
	 * 
	 * @param source
	 * @param target
	 */
	public static void copyProperties(Object source, Object target) {
		BeanUtils.copyProperties(source, target);
	}

	/**
	 * @param date
	 * @return the LocalDateTime date
	 */
	public static LocalDateTime getLocalDateTimeFromString(String date) {

		LocalDateTime dateTime = null;
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
			dateTime = LocalDateTime.parse(date, formatter);
		} catch (Exception e) {
			return null;
		}

		return dateTime;
	}
	
	/**
	 * Get a random number between @min and @max
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	public static BigDecimal getRandomBigDecimalBetween(BigDecimal min, BigDecimal max) {
		
        if (min.compareTo(max) >= 0) {
            throw new IllegalArgumentException("Max must be greater than min.");
        }
        
        SecureRandom random = new SecureRandom();
        double randomDouble = random.nextDouble();

        BigDecimal range = max.subtract(min);

        BigDecimal randomValue = range.multiply(BigDecimal.valueOf(randomDouble)).add(min);

        return randomValue.setScale(1, RoundingMode.HALF_UP);  
    }
	
	/**
	 * Convert bytes in GigaBytes
	 * 
	 * @param bytes
	 * @return
	 */
	public static BigDecimal getGigaBytesFromBytes(BigDecimal bytes) {

		BigDecimal bytesInGB = new BigDecimal(Constants.GB_IN_BYTES);

		return bytes.divide(bytesInGB, 2, RoundingMode.HALF_UP);

	}

}
