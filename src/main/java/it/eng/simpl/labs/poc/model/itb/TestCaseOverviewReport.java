package it.eng.simpl.labs.poc.model.itb;

import java.util.ArrayList;
import java.util.List;

import it.eng.simpl.labs.poc.enumeration.ItbResultEnum;
import lombok.Data;

@Data
public class TestCaseOverviewReport {

	private String id;

	private TestCaseOverviewReportMetadata metadata;

	private String startTime;

	private String endTime;

	private ItbResultEnum result;

	private String message;

	private List<TestCaseOverviewReportStepsInner> steps = new ArrayList<>();

}
