package it.eng.simpl.labs.poc.model;

import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;
import it.eng.simpl.labs.poc.model.itb.SessionCreationInformation;
import lombok.Builder;
import lombok.Data;

/**
 * Information on the created test sessions.
 */
@Data
@Builder
public class TestSessionStartResponse {

	@Schema(name = "createdSessions", description = "The created tests session list")
	private List<SessionCreationInformation> createdSessions;
	
	@Schema(name = "executionProgressive", description = "The tests session execution progressive")
	private String executionProgressive;
	
}

