package it.eng.simpl.labs.poc.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.eng.simpl.labs.poc.entity.CategoryEntity;
import it.eng.simpl.labs.poc.entity.DataspaceEntity;
import it.eng.simpl.labs.poc.entity.NodeEntity;
import it.eng.simpl.labs.poc.enumeration.DataspaceStatusEnum;
import it.eng.simpl.labs.poc.exception.GenericException;
import it.eng.simpl.labs.poc.mapper.DataspaceMapper;
import it.eng.simpl.labs.poc.model.Dataspace;
import it.eng.simpl.labs.poc.model.User;
import it.eng.simpl.labs.poc.repository.ComponentRepository;
import it.eng.simpl.labs.poc.repository.DataspaceRepository;
import it.eng.simpl.labs.poc.service.interf.IDataspaceService;
import it.eng.simpl.labs.poc.service.interf.IDeploymentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
@AllArgsConstructor
public class DataspaceService implements IDataspaceService{
	
	private final DataspaceRepository dataspaceRepository;
	
	private final DataspaceMapper dataspaceMapper;
	
	private final IDeploymentService deploymentService;
	
	private final ComponentRepository componentRepository;
	
	@Override
	public List<Dataspace> findAll(User user) {
		log.info("Request to find all dataspace from user {}",user.getUsername());
		List<DataspaceEntity> dataspaceEntityList=dataspaceRepository.findAllByUserId(user.getUsername());
		return dataspaceMapper.createFromEntities(dataspaceEntityList).toList();
	}
	
	@Override
	public List<Dataspace> findAll() {
		List<DataspaceEntity> dataspaceEntityList=dataspaceRepository.findAll();
		return dataspaceMapper.createFromEntities(dataspaceEntityList).toList();
	}
	
	@Override
	public Dataspace save(Dataspace dataspace, User user) {
		log.info("Request to create a new dataspace from user {}",user.getUsername());
		dataspace.setUser(user);
		dataspace.setStatus(DataspaceStatusEnum.REQUESTED);
		DataspaceEntity entitySaved=dataspaceRepository.saveAndFlush(dataspaceMapper.createFromDto(dataspace));

		for(NodeEntity nodeEntity : entitySaved.getNodeList()) {
			for(CategoryEntity categoryEntity : nodeEntity.getCategoryList()) {
				componentRepository.saveAllAndFlush(categoryEntity.getComponentList());
			}
		}
		
		Dataspace dataspaceSaved=dataspaceMapper.createFromEntity(entitySaved);
		deploymentService.initDataspaceCreation(dataspaceSaved);
		return dataspaceSaved;
	}
	
	@Override
	public Dataspace delete(Long dataspaceId, User user) {
		//Check if the user owns the dataspace
		DataspaceEntity entity=dataspaceRepository.findByIdAndUserId(dataspaceId,user.getUsername());
		if (isPossibleToDelete(entity)) {
			entity.setStatus(DataspaceStatusEnum.REQUESTED_DELETION);
			Dataspace dataspaceToDelete=dataspaceMapper.createFromEntity(entity);
			deploymentService.initDataspaceDeletion(dataspaceToDelete);
			return dataspaceToDelete;
		} else {
			throw new GenericException("Data space cannot be deleted");
		}
	}
	
	/**
	 * Check if Dataspace can be deleted
	 * 
	 * @param entity
	 * @return
	 */
	private boolean isPossibleToDelete(DataspaceEntity entity) {
		return (entity!=null && (
				DataspaceStatusEnum.RUNNING.equals(entity.getStatus()) ||
				DataspaceStatusEnum.ERROR.equals(entity.getStatus())
			));
	}

	@Override
	public Dataspace findById(Long id) {
		
		Optional<DataspaceEntity> dataspaceEntity = dataspaceRepository.findById(id);
		
		return dataspaceMapper.createFromEntity(dataspaceEntity.orElse(null));
	}

}
