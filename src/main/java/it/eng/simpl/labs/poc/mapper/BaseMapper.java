package it.eng.simpl.labs.poc.mapper;

import java.util.Collection;
import java.util.stream.Stream;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class BaseMapper<D,E> {
	
	protected ObjectMapper mapper;
	
	public abstract E createFromDto(D dto);

	public abstract D createFromEntity(E entity);
	
	public Stream<E> createFromDtos(final Collection<D> dtos) {
		return dtos.stream().map(this::createFromDto);
	}
	public Stream<D> createFromEntities(final Collection<E> entities) {
		return entities.stream().map(this::createFromEntity);
	}
	public void copyProperties(Object source, Object target) {
		BeanUtils.copyProperties(source, target);
	}

}
