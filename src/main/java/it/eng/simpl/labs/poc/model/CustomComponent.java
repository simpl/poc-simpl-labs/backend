package it.eng.simpl.labs.poc.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import io.swagger.v3.oas.annotations.media.Schema;
import it.eng.simpl.labs.poc.utils.Constants;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomComponent {
	
	@Schema(name = "id", description = "The custom component id")
	private Long id;
	
	@Schema(name = "version", description = "The custom component version")
	private String version;
	
	@Schema(name = "name", description = "The custom component name")
	private String name;
	
	@Schema(name = "description", description = "The custom component description")
	private String description;
	
	@Schema(name = "specificationDescription", description = "The custom component specification description")
	private String specificationDescription;
	
	@Schema(name = "specificationId", description = "The custom component specification id")
	private Long specificationId;
	
	@Schema(name = "specificationLastUpdate", description = "The custom component specification last update time")
	private LocalDateTime specificationLastUpdate;

	@Schema(name = "nodeType", description = "The custom component node type")
	private String nodeType;
	
	@Schema(name = "category", description = "The custom component category")
	private String category;
	
	@Builder.Default
	@Schema(name = "status", description = "The custom component status")
	private String status = Constants.UNDEFINED_LOWERCASE;
	
	@Schema(name = "resultSuccess", description = "The percentage of successfully executed tests")
	private BigDecimal resultSuccess;
	
	@Schema(name = "resultError", description = "The percentage of test executed with error")
	private BigDecimal resultError;
	
	@Schema(name = "resultNotExecuted", description = "The percentage of not executed tests")
	private BigDecimal resultNotExecuted;
	
	@Schema(name = "system", description = "The custom component system")
	private String system;
	
	@Schema(name = "customImage", description = "The custom component image")
	private String customImage;
	
}
