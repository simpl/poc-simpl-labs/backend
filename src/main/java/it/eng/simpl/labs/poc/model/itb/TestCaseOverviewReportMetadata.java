package it.eng.simpl.labs.poc.model.itb;

import lombok.Data;

@Data
public class TestCaseOverviewReportMetadata {
	
  private String name;

  private String description;

}

