package it.eng.simpl.labs.poc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.eng.simpl.labs.poc.exception.GenericException;
import it.eng.simpl.labs.poc.model.TestSessionStartRequest;
import it.eng.simpl.labs.poc.model.TestSessionStartResponse;
import it.eng.simpl.labs.poc.model.itb.SessionStartRequest;
import it.eng.simpl.labs.poc.model.itb.SessionStartResponse;
import it.eng.simpl.labs.poc.model.itb.SessionStatusRequest;
import it.eng.simpl.labs.poc.model.itb.SessionStatusResponse;
import it.eng.simpl.labs.poc.model.itb.SessionStopRequest;
import it.eng.simpl.labs.poc.model.itb.SessionStopResponse;
import it.eng.simpl.labs.poc.service.interf.IItbClientService;
import it.eng.simpl.labs.poc.service.interf.IItbTestSessionsManagementService;
import it.eng.simpl.labs.poc.utils.GenericUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class ItbTestSessionsManagementService implements IItbTestSessionsManagementService{

	private static final String TESTS_STOP = "/tests/stop";

	private static final String TESTS_STATUS = "/tests/status";

	private static final String TESTS_START = "/tests/start";

	@Value("${itb.system1.api.key}")
	protected String sistem1ApiKey;

	@Value("${itb.system2.api.key}")
	protected String sistem2ApiKey;
	
	@Value("${itb.system1.name}")
	protected String sistem1name;

	@Value("${itb.system2.name}")
	protected String sistem2name;

	@Value("${itb.actor.api.key}")
	protected String actorApiKey;

	private final IItbClientService itbClientService;
	
	@Transactional
	@Override
	public TestSessionStartResponse startTestSession(TestSessionStartRequest localSessionStartRequest) {

		if(localSessionStartRequest == null) {
			throw new GenericException("Uncorrect start request");
		}

		SessionStartRequest sessionStartRequest = new SessionStartRequest();
		GenericUtils.copyProperties(localSessionStartRequest, sessionStartRequest);
		sessionStartRequest.setActor(actorApiKey);
		if (sistem1name.equals(localSessionStartRequest.getSystem())) {
			sessionStartRequest.setSystem(sistem1ApiKey);
		} else {
			sessionStartRequest.setSystem(sistem2ApiKey);
		}

		log.debug("Sending test session start request to ITB system: {}", sessionStartRequest);

		SessionStartResponse sessionStartResponse = null;
		try {
			sessionStartResponse = (SessionStartResponse) itbClientService.startTestSession(sessionStartRequest, TESTS_START);
			log.debug("ITB system response : {}", sessionStartResponse);
		} catch (Exception e) {
			throw new GenericException("Test session start request executed with error", e);

		}
		if(sessionStartResponse == null || sessionStartResponse.getCreatedSessions().isEmpty()) {
			throw new GenericException("Tests session start request executed with error");
		}

		TestSessionStartResponse testSessionStartResponse = TestSessionStartResponse.builder().build();

		GenericUtils.copyProperties(sessionStartResponse, testSessionStartResponse);

		return testSessionStartResponse;
	}
	

	@Override
	@Transactional
	public SessionStopResponse stopTestSession(SessionStopRequest sessionStopRequest) {

		if(sessionStopRequest == null) {
			throw new GenericException("Uncorrect stop request");
		}

		log.debug("Sending test session stop request to ITB system: {}", sessionStopRequest);

		try {
			itbClientService.stopTestSession(sessionStopRequest, TESTS_STOP);
			log.debug("Test session stop request executed successfully");
		} catch (Exception e) {
			throw new GenericException("Test session stop request executed with error", e);
		}

		return SessionStopResponse.builder().build();

	}
		
	@Override
	public SessionStatusResponse getSessionStatus(List<String> testSessionsList, Boolean withLogs, Boolean withReports) {
		
		if(testSessionsList == null || testSessionsList.isEmpty()) {
			throw new GenericException("Test sessions list empty");
		}
		
		SessionStatusRequest sessionStatusRequest = new SessionStatusRequest();
		sessionStatusRequest.setWithLogs(withLogs);
		sessionStatusRequest.setWithReports(withReports);
		sessionStatusRequest.setSession(testSessionsList);

		log.info("Sending test sessions status request to ITB system: {}", sessionStatusRequest);
		
		SessionStatusResponse sessionStatusResponse = null;
		
		try {
			sessionStatusResponse = (SessionStatusResponse) itbClientService.getSessionStatus(sessionStatusRequest, TESTS_STATUS);
			log.info("ITB system response : {}", sessionStatusResponse);
		} catch (Exception e) {
			throw new GenericException("Test sessions status request executed with error");
		}
		
		return sessionStatusResponse;
	}

}
