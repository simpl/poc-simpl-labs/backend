package it.eng.simpl.labs.poc.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import it.eng.simpl.labs.poc.model.GenericResponse;
import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class RestfulExceptionHandler {
	
	@ExceptionHandler(value = Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public GenericResponse handleCustomerAlreadyExistsException(Exception ex) {
		log.error("Generic error: " + ex.getMessage(), ex);
	    return new GenericResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
	}

}
