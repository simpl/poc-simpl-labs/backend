package it.eng.simpl.labs.poc.model;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4650146726683090459L;
	
	private String username;
	private String email;
	private String name;
}
