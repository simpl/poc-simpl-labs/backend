package it.eng.simpl.labs.poc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.task.DelegatingSecurityContextAsyncTaskExecutor;

/**
 * Basic configuration for async jobs
 */
@Configuration
@EnableAsync
public class AsyncConfig{
    
    @Bean
    public TaskExecutor threadPoolTaskExecutor() {

        final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(10);
        executor.setMaxPoolSize(100);
        executor.setQueueCapacity(50);
        executor.setThreadNamePrefix("AsyncTaskExecutor-");
        executor.initialize();

        return new DelegatingSecurityContextAsyncTaskExecutor(executor) {
            @SuppressWarnings("unused")
            public void shutdown() {
                executor.destroy();
            }
        };
    }

}

