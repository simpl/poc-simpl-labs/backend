package it.eng.simpl.labs.poc.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TestCaseExecutionReport {

	@Schema(name = "report", description = "The test case execution report")
	private String report;

}
