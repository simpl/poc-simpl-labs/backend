package it.eng.simpl.labs.poc.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import it.eng.simpl.labs.poc.entity.HistoricalMetricsEntity;

@Repository
public interface HistoricalMetricsRepository extends BaseRepository<HistoricalMetricsEntity, UUID> {

	public void deleteByCreateDateLessThan(LocalDateTime date);

	public List<HistoricalMetricsEntity> deleteByComponentIdIn(List<Long> componentIdList); 

	String QUERY_SECTION_1 =
			"SELECT date, " +
					"ROUND(SUM(cpu_allocated),10), " +
					"ROUND(SUM(cpu_used),10), " +
					"ROUND(SUM(memory_allocated),10), " +
					"ROUND(SUM(memory_used),10), " +
					"ROUND(SUM(storage_allocated),10), " +
					"ROUND(SUM(storage_used),10) " +
					"FROM ( " +
					"  SELECT DATE(c.create_date) AS date, " +
					"         c.component_id, " +
					"         ROUND(AVG(c.cpu_allocated), 10) AS cpu_allocated, " +
					"         ROUND(AVG(c.cpu_used), 10) AS cpu_used, " +
					"         ROUND(AVG(c.memory_allocated), 10) AS memory_allocated, " +
					"         ROUND(AVG(c.memory_used), 10) AS memory_used, " +
					"         ROUND(AVG(c.storage_allocated), 10) AS storage_allocated, " +
					"         ROUND(AVG(c.storage_used), 10) AS storage_used " +
					"  FROM historical_metrics_entity c INNER JOIN dataspace_entity d ON (c.dataspace_id = d.id)" +
					"  WHERE " +
					"    DATE(c.create_date) >= ?1 " +
					"    AND DATE(c.create_date) <= ?2 ";
	
	String QUERY_SECTION_2 =
					"  GROUP BY DATE(c.create_date), c.component_id) AS subquery " +
					"GROUP BY date " +
					"ORDER BY date ASC";
			
	@Query(nativeQuery = true, value = QUERY_SECTION_1 + " AND d.user_id = ?3 " + QUERY_SECTION_2)
	public List<Object[]> getGeneralHistoricalMetrics(LocalDate startDate, LocalDate endDate, String user);
	
	@Query(nativeQuery = true, value = QUERY_SECTION_1 + " AND d.id = ?3 " + QUERY_SECTION_2)
	public List<Object[]> getDataspacesHistoricalMetrics(LocalDate startDate, LocalDate endDate, Long id);
	
	@Query(nativeQuery = true, value = QUERY_SECTION_1 + " AND c.node_id = ?3 " + QUERY_SECTION_2)
	public List<Object[]> getNodesHistoricalMetrics(LocalDate startDate, LocalDate endDate, Long id);
	
	@Query(nativeQuery = true, value = QUERY_SECTION_1 + " AND c.category_id = ?3 " + QUERY_SECTION_2)
	public List<Object[]> getCategoriesHistoricalMetrics(LocalDate startDate, LocalDate endDate, Long id);
	
	@Query(nativeQuery = true, value = QUERY_SECTION_1 + " AND c.component_id = ?3 " + QUERY_SECTION_2)
	public List<Object[]> getComponentsHistoricalMetrics(LocalDate startDate, LocalDate endDate, Long id);
	
    @Modifying 
    @Transactional 
    @Query("DELETE FROM HistoricalMetricsEntity a WHERE NOT EXISTS (SELECT 1 FROM DataspaceEntity b WHERE a.dataspaceId = b.id)")
    public void deleteOrphans();

}