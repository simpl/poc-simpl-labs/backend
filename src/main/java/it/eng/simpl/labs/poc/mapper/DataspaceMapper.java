package it.eng.simpl.labs.poc.mapper;

import java.util.List;

import org.springframework.stereotype.Component;

import it.eng.simpl.labs.poc.entity.DataspaceEntity;
import it.eng.simpl.labs.poc.entity.NodeEntity;
import it.eng.simpl.labs.poc.model.Dataspace;
import it.eng.simpl.labs.poc.model.User;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class DataspaceMapper extends BaseMapper<Dataspace, DataspaceEntity> {

	private final NodeMapper nodeMapper;

	@Override
	public DataspaceEntity createFromDto(Dataspace dto) {
		DataspaceEntity entity = new DataspaceEntity();
		copyProperties(dto, entity);
		List<NodeEntity> nodeList = nodeMapper.createFromDtos(dto.getNodeList()).toList();
		nodeList.forEach(node -> node.setDataspace(entity));
		entity.setNodeList(nodeList);
		entity.setUserId(dto.getUser().getUsername());
		return entity;
	}

	@Override
	public Dataspace createFromEntity(DataspaceEntity entity) {
		Dataspace dto = Dataspace.builder().build();
		copyProperties(entity, dto);
		dto.setNodeList(nodeMapper.createFromEntities(entity.getNodeList()).toList());
		dto.setUser(User.builder().username(entity.getUserId()).build());
		dto.setCreateDate(entity.getCreateDate());
		return dto;
	}

}
