package it.eng.simpl.labs.poc.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.eng.simpl.labs.poc.entity.CurrentMetricsEntity;
import it.eng.simpl.labs.poc.repository.CurrentMetricsRepository;
import it.eng.simpl.labs.poc.repository.HistoricalMetricsRepository;
import it.eng.simpl.labs.poc.service.interf.IK8sClusterManagementService;
import it.eng.simpl.labs.poc.service.interf.IMetricsManagementService;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class MetricsManagementService implements IMetricsManagementService {

	private final HistoricalMetricsRepository historicalMetricsRepository;

	private final CurrentMetricsRepository currentMetricsRepository;

	private final IK8sClusterManagementService k8sClusterManagementService; 

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Override
	public void deleteOrphans() {

		historicalMetricsRepository.deleteOrphans();

	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Override
	public void deleteOldMetrics(LocalDateTime date) {

		historicalMetricsRepository.deleteByCreateDateLessThan(date);

	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Override
	public void deleteByCreateDateLessThan(LocalDateTime date) {

		historicalMetricsRepository.deleteByCreateDateLessThan(date);

	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Override
	public void updateMetrics() {

		historicalMetricsRepository.saveAllAndFlush(k8sClusterManagementService.getHistoricalMetrics());

	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Override
	public List<CurrentMetricsEntity> updateBufferedMetrics() {
		
		currentMetricsRepository.deleteAllMetrics();

		List<CurrentMetricsEntity> currentMetricsEntityList = k8sClusterManagementService.getCurrentMetrics();

		return currentMetricsRepository.saveAllAndFlush(currentMetricsEntityList);

	}

}
