package it.eng.simpl.labs.poc.model;

import java.time.LocalDateTime;

import io.swagger.v3.oas.annotations.media.Schema;
import it.eng.simpl.labs.poc.utils.Constants;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TestExecution {
	
	@Schema(name = "id", description = "The test execution id")
	private Long id;

	@Schema(name = "startDate", description = "The test execution start date")
	private LocalDateTime startDate;
	
	@Schema(name = "endDate", description = "The test execution end date")
	private LocalDateTime endDate;
	
	@Builder.Default
	@Schema(name = "status", description = "The test execution status")
	private String status = Constants.UNDEFINED_LOWERCASE;
	
	@Schema(name = "totalTestsNumber", description = "The total number of executed test")
	private Long totalTestsNumber;
	
	@Schema(name = "passedTestsNumber", description = "The total number of passed test")
	private Long passedTestsNumber;
	
	@Schema(name = "failedTestsNumber", description = "The total number of failed test")
	private Long failedTestsNumber;
	
	@Schema(name = "undefinedTestsNumber", description = "The total number of test in undefined status")
	private Long undefinedTestsNumber;
	
	@Schema(name = "componentId", description = "The custom component id")
	private String componentId;
	
	@Schema(name = "componentName", description = "The custom component name")
	private String componentName;

	@Schema(name = "specificationId", description = "The custom component specification id")
	private Long specificationId;
	
	@Schema(name = "specificationName", description = "The custom component specification name")
	private String specificationName;
	
	@Schema(name = "specificationLastUpdate", description = "The custom component specification last update time")
	private LocalDateTime specificationLastUpdate;
	
}
