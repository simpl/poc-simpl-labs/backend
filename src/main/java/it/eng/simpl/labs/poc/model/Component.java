package it.eng.simpl.labs.poc.model;

import it.eng.simpl.labs.poc.enumeration.ComponentTypeEnum;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class Component {
	
	private Long id;
	
	private ComponentTypeEnum type;
	
	@Builder.Default
	private String param1="value1";
	
	@Builder.Default
	private String param2="value2";
	
	@Builder.Default
	private String param3="value3";
	
	@Builder.Default
	private String param4="value4";
	
	@Builder.Default
	private String param5="value5";
	
	@Builder.Default
	private String param6="value6";
	
	private String customImage;
	
	private Long customComponentId;

}
