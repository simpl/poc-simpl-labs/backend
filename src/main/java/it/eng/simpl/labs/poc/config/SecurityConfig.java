package it.eng.simpl.labs.poc.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import it.eng.simpl.labs.poc.utils.JwtUtil;
import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableWebSecurity
@Slf4j
public class SecurityConfig {

	@Value("${cors.header}")
	private String corsHeader;
	@Value("${cors.origin}")
	private List<String> corsOrigin;
	@Value("${cors.method}")
	private List<String> corsMethod;

	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		log.info("SecurityFilterChain configuration method");
		http.cors(Customizer.withDefaults())
				.authorizeHttpRequests(
						authz -> authz.requestMatchers(getAntPattern()).permitAll().anyRequest().authenticated())
				.oauth2ResourceServer(oauth2 -> oauth2.jwt(
						jwt -> jwt.jwtAuthenticationConverter(JwtUtil::createJwtUser))
		);
		return http.build();
	}

	@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		log.info("Executing corsConfigurationSource method");

		CorsConfiguration corsConfiguration = new CorsConfiguration();

		corsConfiguration.addAllowedHeader(corsHeader);
		corsConfiguration.setAllowedOriginPatterns(corsOrigin);
		corsConfiguration.setAllowedMethods(corsMethod);
		corsConfiguration.setAllowCredentials(true);

		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", corsConfiguration);
		return source;
	}

	public String[] getAntPattern() {
		return Arrays.asList("/actuator/health", "/v3/api-docs/**", "/public/**", "/swagger-ui/**", "/webjars/**",
				"/configuration/ui", "/swagger-resources", "/configuration/security", "/swagger-ui.html",
				"/swagger-resources/configuration/ui", "/swagger-ui.html", "/swagger-resources/configuration/security")
				.toArray(new String[0]);
	}
	
}