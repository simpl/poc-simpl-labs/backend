package it.eng.simpl.labs.poc.service.interf;

import it.eng.simpl.labs.poc.model.itb.ISessionRequest;
import it.eng.simpl.labs.poc.model.itb.ISessionResponse;

public interface IItbClientService {
	
	public ISessionResponse startTestSession(ISessionRequest request, String finalUri);
	
	public ISessionResponse stopTestSession(ISessionRequest request, String finalUri);

	public ISessionResponse getSessionStatus(ISessionRequest request, String finalUri);
	
}
