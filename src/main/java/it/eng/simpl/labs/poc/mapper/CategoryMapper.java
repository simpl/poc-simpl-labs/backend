package it.eng.simpl.labs.poc.mapper;

import java.util.List;

import org.springframework.stereotype.Component;

import it.eng.simpl.labs.poc.entity.CategoryEntity;
import it.eng.simpl.labs.poc.entity.ComponentEntity;
import it.eng.simpl.labs.poc.model.Category;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class CategoryMapper extends BaseMapper<Category, CategoryEntity> {

	private final ComponentMapper componentMapper;

	@Override
	public CategoryEntity createFromDto(Category dto) {

		CategoryEntity entity = new CategoryEntity();

		copyProperties(dto, entity);
		
		List<ComponentEntity> componentEntityList = componentMapper.createFromDtos(dto.getComponentList()).toList();
		
		componentEntityList.forEach(component -> component.setCategory(entity));
		
		entity.setComponentList(componentEntityList);

		return entity;
	}

	@Override
	public Category createFromEntity(CategoryEntity entity) {
		
		Category dto = Category.builder().build();

		copyProperties(entity, dto);
		
		dto.setComponentList(componentMapper.createFromEntities(entity.getComponentList()).toList());
		
		return dto;
	}

}
