package it.eng.simpl.labs.poc.entity;

import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.ConstraintMode;
import jakarta.persistence.Entity;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class CurrentMetricsEntity extends BaseMetricsEntity {

    @Id
    @GeneratedValue
    @Column(columnDefinition = "UUID")
    private UUID id;
	
	@ManyToOne
	@JoinColumn(name = "component_id", nullable = false, insertable = false, updatable = false,
	foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
	private ComponentEntity component;
	
}
