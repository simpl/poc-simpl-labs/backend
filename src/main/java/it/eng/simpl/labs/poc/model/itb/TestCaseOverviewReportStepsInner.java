package it.eng.simpl.labs.poc.model.itb;

import lombok.Data;

@Data
public class TestCaseOverviewReportStepsInner {

	private TestCaseOverviewReportStepsInnerStep step;

}
