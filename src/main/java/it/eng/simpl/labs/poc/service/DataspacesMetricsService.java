package it.eng.simpl.labs.poc.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.eng.simpl.labs.poc.entity.CurrentMetricsEntity;
import it.eng.simpl.labs.poc.model.CurrentConsumption;
import it.eng.simpl.labs.poc.model.HistoricalConsumption;
import it.eng.simpl.labs.poc.repository.CurrentMetricsRepository;
import it.eng.simpl.labs.poc.repository.HistoricalMetricsRepository;
import it.eng.simpl.labs.poc.service.interf.IDataspacesMetricsService;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class DataspacesMetricsService extends AbstractMetricsService implements IDataspacesMetricsService {

	private final HistoricalMetricsRepository historicalMetricsRepository;
	
	private final CurrentMetricsRepository currentMetricsRepository;
		
	@Override
    public Page<CurrentConsumption> getCurrentMetrics(Long id, Pageable pageable) {
		
        List<CurrentMetricsEntity> currentMetricsEntityListGlobal = currentMetricsRepository.findByDataspaceId(id);
        
        Page<CurrentMetricsEntity> currentMetricsEntityPage = currentMetricsRepository.findByDataspaceId(id, pageable);
        
        return getCurrentMetrics(currentMetricsEntityListGlobal, currentMetricsEntityPage);
    }
	
	@Override
	public List<CurrentConsumption> getCurrentMetrics(Long id) {
        
        return getCurrentMetrics(currentMetricsRepository.findByDataspaceId(id));
        
	}
	
	@Override
	public List<HistoricalConsumption> getHistoricalCpuMetrics(Long id, LocalDate startDate, LocalDate endDate) {
		List<Object[]> list = historicalMetricsRepository.getDataspacesHistoricalMetrics(startDate, endDate, id);
		return getHistoricalCpuMetrics(list);
	}
	
	@Override
	public List<HistoricalConsumption> getHistoricalMemoryMetrics(Long id, LocalDate startDate, LocalDate endDate) {
		List<Object[]> list = historicalMetricsRepository.getDataspacesHistoricalMetrics(startDate, endDate, id);
		return getHistoricalMemoryMetrics(list);
	}

	@Override
	public List<HistoricalConsumption> getHistoricalStorageMetrics(Long id, LocalDate startDate, LocalDate endDate) {
		List<Object[]> list = historicalMetricsRepository.getDataspacesHistoricalMetrics(startDate, endDate, id);
		return getHistoricalStorageMetrics(list);
	}
		
}
