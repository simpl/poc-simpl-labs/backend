package it.eng.simpl.labs.poc.enumeration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ItbStepResultEnum {

	SUCCESS("SUCCESS"),

	FAILURE("FAILURE"),

	WARNING("WARNING"),

	UNDEFINED("UNDEFINED");

	private String value;

	ItbStepResultEnum(String value) {
		this.value = value;
	}

	@JsonValue
	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	@JsonCreator
	public static ItbStepResultEnum fromValue(String value) {
		for (ItbStepResultEnum b : ItbStepResultEnum.values()) {
			if (b.value.equals(value)) {
				return b;
			}
		}
		throw new IllegalArgumentException("Unexpected value '" + value + "'");
	}
}