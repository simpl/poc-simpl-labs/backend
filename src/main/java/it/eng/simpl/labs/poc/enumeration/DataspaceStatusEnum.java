package it.eng.simpl.labs.poc.enumeration;

public enum DataspaceStatusEnum {
	REQUESTED,
	INSTALLING,
	RUNNING,
	PAUSED,
	ERROR,
	REQUESTED_DELETION,
	DELETING;
}
