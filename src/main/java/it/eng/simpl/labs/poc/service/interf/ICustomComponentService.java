package it.eng.simpl.labs.poc.service.interf;

import java.util.List;

import it.eng.simpl.labs.poc.model.CustomComponent;
import it.eng.simpl.labs.poc.model.Dataspace;
import it.eng.simpl.labs.poc.model.User;

public interface ICustomComponentService {

	/**
	 * @return
	 */
	public List<CustomComponent> findAll();
	
	/**
	 * @param id
	 * @return
	 */
	public CustomComponent findById(Long customComponentId);
	
	/**
	 * 
	 * Gets all Dataspaces that contain the component
	 * 
	 * @param componentId
	 * @param user
	 * @return
	 */
	public List<Dataspace> getDataspaces(Long componentId, User user);
}
