package it.eng.simpl.labs.poc.model.itb;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class SessionStopRequest implements ISessionRequest {

	private List<String> session = new ArrayList<>();

}

