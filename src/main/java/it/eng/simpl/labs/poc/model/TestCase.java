package it.eng.simpl.labs.poc.model;

import java.time.LocalDateTime;
import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;
import it.eng.simpl.labs.poc.utils.Constants;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TestCase {

	@Schema(name = "id", description = "The test case id")
	private String id;

	@Schema(name = "name", description = "The test case name")
	private String name;

	@Schema(name = "description", description = "The test case description")
	private String description;
	
	@Schema(name = "version", description = "The test case version")
	private String version;

	@Builder.Default
	@Schema(name = "status", description = "The test case description")
	private String status = Constants.UNDEFINED_LOWERCASE;
	
	@Schema(name = "startDate", description = "The test case exectuion start time")
	private LocalDateTime startDate;
	
	@Schema(name = "endDate", description = "The test case exectuion end time")
	private LocalDateTime endDate;
	
	@Schema(name = "session", description = "The test case exectuion session")
	private String session;
	
	@Schema(name = "testSuiteId", description = "The test suite id")
	private String testSuiteId;
	
	@Schema(name = "testSuiteDescription", description = "The test suite description")
	private String testSuiteDescription;
	
	@Schema(name = "testSuiteLastUpdate", description = "The test suite last update time")
	private LocalDateTime testSuiteLastUpdate;
	
	@Schema(name = "stepsList", description = "The test case steps")
	private List<TestCaseStep> stepsList;

}
