package it.eng.simpl.labs.poc.service.interf;

import java.util.List;

import it.eng.simpl.labs.poc.model.TestSessionStartRequest;
import it.eng.simpl.labs.poc.model.TestSessionStartResponse;
import it.eng.simpl.labs.poc.model.itb.SessionStatusResponse;
import it.eng.simpl.labs.poc.model.itb.SessionStopRequest;
import it.eng.simpl.labs.poc.model.itb.SessionStopResponse;

public interface IItbTestSessionsManagementService {

	/**
	 * Send the test session start request to ITB system
	 * 
	 * @param localSessionStartRequest
	 * @return
	 */
	public TestSessionStartResponse startTestSession(TestSessionStartRequest localSessionStartRequest);

	/**
	 * 
 	 * Send the test session stop request to ITB system
 	 * 
	 * @param sessionStopRequest
	 * @return
	 */
	public SessionStopResponse stopTestSession(SessionStopRequest sessionStopRequest);
	
	/**
	 * Get the test sessions execution status from ITB
	 * 
	 * @param testSessionsList
	 * @param withLogs
	 * @param withReports
	 * @return
	 */
	public SessionStatusResponse getSessionStatus(List<String> testSessionsList, Boolean withLogs, Boolean withReports);
		
}
