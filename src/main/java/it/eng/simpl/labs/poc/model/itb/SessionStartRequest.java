package it.eng.simpl.labs.poc.model.itb;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class SessionStartRequest implements ISessionRequest {

	private String system;

	private String actor;

	private Boolean forceSequentialExecution = false;

	private List<String> testSuite = new ArrayList<>();

	private List<String> testCase = new ArrayList<>();

	private List<InputMapping> inputMapping = new ArrayList<>();

}