package it.eng.simpl.labs.poc.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import it.eng.simpl.labs.poc.entity.TestExecutionEntity;

@Repository
public interface TestExecutionRepository extends BaseRepository<TestExecutionEntity, Long> {
	
	public List<TestExecutionEntity> findByCustomComponentIdAndStatusInOrderByIdAsc(Long componentId, List<Long> statusList);
	
	public List<TestExecutionEntity> findByCustomComponentIdAndCustomComponentSpecificationIdOrderByIdDesc(Long componentId, Long specificationId);

}
