package it.eng.simpl.labs.poc.entity;

import java.util.List;

import it.eng.simpl.labs.poc.enumeration.CategoryTypeEnum;
import it.eng.simpl.labs.poc.enumeration.NodeTypeEnum;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(uniqueConstraints = { @UniqueConstraint(name = "UniqueNameAndVersions", columnNames = { "name", "version" }) })
public class CustomComponentEntity extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	@Min(0)
	@Max(9223372036854775807l)
	private Long id;
	
	@Column(nullable = false)
	private String version;

	@Column(nullable = false)
	private String name;
	
	@Column
	private String description;
	
	@Column(name = "specification_id", nullable = false)
	private Long specificationId;

	@ManyToOne
	@JoinColumn(name = "specification_id", nullable = false, insertable = false, updatable = false)
	private SpecificationEntity specification;

	@Enumerated
	private NodeTypeEnum nodeType;

	@Enumerated
	private CategoryTypeEnum category;

	@OneToMany(mappedBy = "customComponent")
	@OrderBy("id ASC")
	private List<TestExecutionEntity> testexecutionList;
	
	@Column(nullable = false)
	private String system;
	
	@Column
	private String customImage;
}
