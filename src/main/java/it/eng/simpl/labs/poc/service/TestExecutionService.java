package it.eng.simpl.labs.poc.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.eng.simpl.labs.poc.entity.TestCaseExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestExecutionEntity;
import it.eng.simpl.labs.poc.enumeration.TestStatusEnum;
import it.eng.simpl.labs.poc.mapper.TestCaseMapper;
import it.eng.simpl.labs.poc.mapper.TestExecutionMapper;
import it.eng.simpl.labs.poc.model.TestCase;
import it.eng.simpl.labs.poc.model.TestCaseExecutionLogs;
import it.eng.simpl.labs.poc.model.TestCaseExecutionReport;
import it.eng.simpl.labs.poc.model.TestExecution;
import it.eng.simpl.labs.poc.model.TestSessionStatus;
import it.eng.simpl.labs.poc.repository.TestCaseExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestExecutionRepository;
import it.eng.simpl.labs.poc.service.interf.ITestExecutionService;
import it.eng.simpl.labs.poc.utils.ItbConfigurationUtils;
import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class TestExecutionService implements ITestExecutionService {

	private final TestExecutionRepository testExecutionRepository;

	private final TestExecutionMapper testExecutionMapper;

	private final TestCaseMapper testCaseMapper;

	private final TestCaseExecutionRepository testCaseExecutionRepository;

	@Override
	public List<TestSessionStatus> getTestHistory(Long componentId) {

		List<TestSessionStatus> testSessionStatusList = new ArrayList<>();

		// Get all test execution with status SUCCESS, FAILURE, UNDEFINED
		List<TestExecutionEntity> testExecutionEntityList = testExecutionRepository
				.findByCustomComponentIdAndStatusInOrderByIdAsc(componentId, Arrays.asList(0L, 1L, 3L));

		if (testExecutionEntityList != null && !testExecutionEntityList.isEmpty()) {
			testExecutionEntityList.stream().forEach(testExecutionEntity -> {
				TestSessionStatus testSessionStatus = TestSessionStatus.builder().build();
				List<TestCase> testCasesList = new ArrayList<>();
				TestExecution testExecution = testExecutionMapper.createFromEntity(testExecutionEntity);

				// Wrapper for mutable counters
				class TestCounter {
					long total = 0;
					long passed = 0;
					long failed = 0;
				}
				TestCounter counters = new TestCounter();

				// Stream through all test suite executions and their test case executions
				testExecutionEntity.getTestSuiteExecutionList().stream().flatMap(
						testSuiteExecutionEntity -> testSuiteExecutionEntity.getTestCaseExecutionList().stream())
						.forEach(testCaseExecutionEntity -> {
							// Counting test results
							counters.total++;
							if (TestStatusEnum.SUCCESS.equals(testCaseExecutionEntity.getStatus())) {
								counters.passed++;
							} else if (TestStatusEnum.FAILURE.equals(testCaseExecutionEntity.getStatus())) {
								counters.failed++;
							}

							// Creating TestCase objects and setting steps' result if needed
							TestCase testCase = testCaseMapper.createFromExecutionEntity(testCaseExecutionEntity);
							if (TestStatusEnum.FAILURE.equals(testCaseExecutionEntity.getStatus())
									|| TestStatusEnum.SUCCESS.equals(testCaseExecutionEntity.getStatus())) {
								testCase.getStepsList()
										.forEach(testCaseStep -> testCaseStep.setResult(ItbConfigurationUtils
												.getTestCaseStepResult(testCaseExecutionEntity.getReport(),
														testCaseStep.getProgressiveId())));
							}
							testCasesList.add(testCase);
						});

				// Set the calculated test execution data
				testExecution.setTotalTestsNumber(counters.total);
				testExecution.setPassedTestsNumber(counters.passed);
				testExecution.setFailedTestsNumber(counters.failed);
				testExecution.setUndefinedTestsNumber(counters.total - (counters.passed + counters.failed));

				// Set the test session status
				testSessionStatus.setTestExecution(testExecution);
				testSessionStatus.setTestCasesList(testCasesList);

				// Add the test session status to the final list
				testSessionStatusList.add(testSessionStatus);
			});
		}

		return testSessionStatusList;

	}

	@Override
	public TestSessionStatus getTestsExecutionStatus(Long specificationId, Long componentId) {

		List<TestCase> testCasesList = new ArrayList<>();
		TestExecution testExecution = TestExecution.builder().build();

		List<TestExecutionEntity> testExecutionEntityList = testExecutionRepository
				.findByCustomComponentIdAndCustomComponentSpecificationIdOrderByIdDesc(componentId, specificationId);

		if (testExecutionEntityList != null && !testExecutionEntityList.isEmpty()) {
			// Mappa il primo elemento della lista in TestExecution
			testExecution = testExecutionMapper.createFromEntity(testExecutionEntityList.get(0));

			// Itera attraverso tutte le TestSuiteExecutionEntity e TestCaseExecutionEntity
			// con Stream
			testExecutionEntityList.get(0).getTestSuiteExecutionList().stream()
					.flatMap(testSuiteExecutionEntity -> testSuiteExecutionEntity.getTestCaseExecutionList().stream())
					.forEach(testCaseExecutionEntity -> {
						// Creazione del TestCase dall'entità
						TestCase testCase = testCaseMapper.createFromExecutionEntity(testCaseExecutionEntity);

						// Verifica se il TestCaseExecutionEntity è completato (SUCCESS o FAILURE)
						if (TestStatusEnum.FAILURE.equals(testCaseExecutionEntity.getStatus())
								|| TestStatusEnum.SUCCESS.equals(testCaseExecutionEntity.getStatus())) {

							// Aggiorna i risultati dei singoli passaggi del test (step)
							testCase.getStepsList()
									.forEach(testCaseStep -> testCaseStep.setResult(ItbConfigurationUtils
											.getTestCaseStepResult(testCaseExecutionEntity.getReport(),
													testCaseStep.getProgressiveId())));
						}

						// Aggiunge il testCase alla lista finale
						testCasesList.add(testCase);
					});
		}

		if (!testCasesList.isEmpty()) {
			if (checkStatus(TestStatusEnum.ONGOING, testCasesList)) {
				testExecution.setStatus(TestStatusEnum.ONGOING.getValue());
			} else if (checkStatus(TestStatusEnum.FAILURE, testCasesList)) {
				testExecution.setStatus(TestStatusEnum.FAILURE.getValue());
			} else if (checkStatus(TestStatusEnum.UNDEFINED, testCasesList)) {
				testExecution.setStatus(TestStatusEnum.UNDEFINED.getValue());
			} else {
				testExecution.setStatus(TestStatusEnum.SUCCESS.getValue());
			}
		}

		return TestSessionStatus.builder().testCasesList(testCasesList).testExecution(testExecution).build();

	}

	@Override
	public TestCaseExecutionLogs getTestCaseExecutionLogs(String sessionId) {

		TestCaseExecutionEntity testCaseExecutionEntity = testCaseExecutionRepository.findBySession(sessionId);

		TestCaseExecutionLogs restCaseExecutionLogs = TestCaseExecutionLogs.builder().build();

		if (testCaseExecutionEntity != null && testCaseExecutionEntity.getLogs() != null
				&& !testCaseExecutionEntity.getLogs().isEmpty()) {
			restCaseExecutionLogs.setLogs(testCaseExecutionEntity.getLogs().toString());
		}

		return restCaseExecutionLogs;
	}

	@Override
	public TestCaseExecutionReport getTestCaseExecutionReport(String sessionId) {

		TestCaseExecutionEntity testCaseExecutionEntity = testCaseExecutionRepository.findBySession(sessionId);

		TestCaseExecutionReport testCaseExecutionReport = TestCaseExecutionReport.builder().build();

		if (testCaseExecutionEntity != null && testCaseExecutionEntity.getReport() != null) {
			testCaseExecutionReport.setReport(testCaseExecutionEntity.getReport());
		}

		return testCaseExecutionReport;
	}

	/**
	 * @param status
	 * @param testCasesList
	 * @return
	 */
	private boolean checkStatus(TestStatusEnum status, List<TestCase> testCasesList) {
		for (TestCase testCase : testCasesList) {
			if (status.getValue().equals(testCase.getStatus())) {
				return true;
			}
		}
		return false;
	}

}
