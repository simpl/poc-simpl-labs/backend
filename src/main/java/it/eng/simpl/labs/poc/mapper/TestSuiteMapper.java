package it.eng.simpl.labs.poc.mapper;

import org.springframework.stereotype.Component;

import it.eng.simpl.labs.poc.entity.TestSuiteEntity;
import it.eng.simpl.labs.poc.model.TestSuite;

@Component
public class TestSuiteMapper extends BaseMapper<TestSuite, TestSuiteEntity> {

	@Override
	public TestSuiteEntity createFromDto(TestSuite dto) {
        throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public TestSuite createFromEntity(TestSuiteEntity entity) {

		TestSuite dto = null;

		if (entity != null) {
			dto = TestSuite.builder().build();
			copyProperties(entity, dto);
		}

		return dto;
	}

}
