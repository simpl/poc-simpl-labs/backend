package it.eng.simpl.labs.poc.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.eng.simpl.labs.poc.entity.TestCaseEntity;
import it.eng.simpl.labs.poc.entity.TestCaseExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestExecutionEntity;
import it.eng.simpl.labs.poc.entity.TestSuiteEntity;
import it.eng.simpl.labs.poc.entity.TestSuiteExecutionEntity;
import it.eng.simpl.labs.poc.exception.GenericException;
import it.eng.simpl.labs.poc.model.TestSessionStartRequest;
import it.eng.simpl.labs.poc.model.TestSessionStartResponse;
import it.eng.simpl.labs.poc.model.itb.SessionCreationInformation;
import it.eng.simpl.labs.poc.model.itb.SessionStopRequest;
import it.eng.simpl.labs.poc.model.itb.SessionStopResponse;
import it.eng.simpl.labs.poc.repository.TestCaseExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestCaseRepository;
import it.eng.simpl.labs.poc.repository.TestExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestSuiteExecutionRepository;
import it.eng.simpl.labs.poc.repository.TestSuiteRepository;
import it.eng.simpl.labs.poc.service.interf.IItbTestSessionsManagementService;
import it.eng.simpl.labs.poc.service.interf.ITestSessionsManagementService;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Data
public class TestSessionsManagementService implements ITestSessionsManagementService{

	private static final String THE_REQUEST_DOESN_T_MATCH_THE_LOCAL_DATA_BASE_CONFIGURATION = "The request doesn't match the local data base configuration";

	private final TestSuiteExecutionRepository testSuiteExecutionRepository;

	private final TestCaseExecutionRepository testCaseExecutionRepository;

	private final TestExecutionRepository testExecutionRepository;

	private final TestCaseRepository testCaseRepository;

	private final TestSuiteRepository testSuiteRepository;
	
	private final IItbTestSessionsManagementService itbTestSessionsManagementService;
	
	private final CustomComponentService customComponentService;

	@Transactional
	@Override
	public TestSessionStartResponse startTestSession(TestSessionStartRequest localSessionStartRequest) {

		// --------------------------------------------
		// Get tests configuration from local data base
		// --------------------------------------------

		List<TestSuiteEntity> globalTestSuiteEntityList = testSuiteRepository.findByIdIn(localSessionStartRequest.getTestSuite());
		
		List<TestCaseEntity> globalTestCaseEntityList = getTestCasesConfigurationForStart(localSessionStartRequest);

		if(globalTestCaseEntityList == null || globalTestCaseEntityList.isEmpty() || globalTestSuiteEntityList == null || globalTestSuiteEntityList.isEmpty()) {
			throw new GenericException(THE_REQUEST_DOESN_T_MATCH_THE_LOCAL_DATA_BASE_CONFIGURATION);
		}
		
		// --------------------------------------------
		// Send START request to ITB system
		// --------------------------------------------
		
		String system = customComponentService.findById(localSessionStartRequest.getCustomComponentId()).getSystem();
		localSessionStartRequest.setSystem(system);
		localSessionStartRequest.setTestCase(globalTestCaseEntityList.stream().map(tc -> tc.getId().getTestCaseId()).toList());
		localSessionStartRequest.setTestSuite(globalTestSuiteEntityList.stream().map(ts -> ts.getId()).toList());	
		TestSessionStartResponse testSessionStartResponse = itbTestSessionsManagementService.startTestSession(localSessionStartRequest);

		// --------------------------------------------------------
		// Create tests execution status objects on local data base
		// --------------------------------------------------------
				
		// Test Execution status
		TestExecutionEntity testExecutionEntity = new TestExecutionEntity();
		testExecutionEntity.setCustomComponentId(localSessionStartRequest.getCustomComponentId());
		testExecutionRepository.saveAndFlush(testExecutionEntity);
						
		for(TestSuiteEntity testSuiteEntity : globalTestSuiteEntityList) {
			
			// Test Suite execution status
			TestSuiteExecutionEntity testSuiteExecutionEntity = new TestSuiteExecutionEntity();
			testSuiteExecutionEntity.setTestSuiteId(testSuiteEntity.getId());
			testSuiteExecutionEntity.setTestExecutionId(testExecutionEntity.getId());
			testSuiteExecutionRepository.saveAndFlush(testSuiteExecutionEntity);
			
			List<TestCaseExecutionEntity> testCaseExecutionEntityList = new ArrayList<>();
			for(TestCaseEntity testCaseEntity : globalTestCaseEntityList) {
				if(testSuiteEntity.getId().equals(testCaseEntity.getTestSuite().getId())) {
					
					TestCaseExecutionEntity testCaseExecutionEntity = new TestCaseExecutionEntity();
					
					// Check if the pair TS-TC is present in the start response too
					boolean found = false;
					for (SessionCreationInformation sessionCreationInformation : testSessionStartResponse.getCreatedSessions()) {
						if (sessionCreationInformation.getTestCase().equals(testCaseEntity.getId().getTestCaseId())
								&& sessionCreationInformation.getTestSuite().equals(testCaseEntity.getId().getTestSuiteId())) {
							found = true;
							testCaseExecutionEntity.setSession(sessionCreationInformation.getSession());
							break;
						}
					}
					if(!found) {
						throw new GenericException("Tests start response doesn't match the local configuration.");						
					}
					
					testCaseExecutionEntity.setTestCaseId(testCaseEntity.getId().getTestCaseId());
					testCaseExecutionEntity.setTestSuiteId(testCaseEntity.getId().getTestSuiteId());
					testCaseExecutionEntity.setTestSuiteExecutionId(testSuiteExecutionEntity.getId());
					testCaseExecutionEntityList.add(testCaseExecutionEntity);
				}
			}
			
			// Test Case execution status
			testCaseExecutionRepository.saveAllAndFlush(testCaseExecutionEntityList);
			
		}

		testSessionStartResponse.setExecutionProgressive(testExecutionEntity.getId()+"");

		return testSessionStartResponse;
	}
	
	@Override
	@Transactional
	public SessionStopResponse stopTestSession(SessionStopRequest sessionStopRequest) {
		
		String executionProgressive = null;
				
		// --------------------------------------------
		// Get tests configuration from local data base
		// --------------------------------------------
		
		for (String session : sessionStopRequest.getSession()) {
			TestCaseExecutionEntity testCaseExecutionEntity = testCaseExecutionRepository.findBySession(session);
			if (testCaseExecutionEntity == null) {
				throw new GenericException(THE_REQUEST_DOESN_T_MATCH_THE_LOCAL_DATA_BASE_CONFIGURATION);
			}
			
			if(testCaseExecutionEntity.getTestSuiteExecution() != null && testCaseExecutionEntity.getTestSuiteExecution().getTestExecution() != null) {
				executionProgressive = testCaseExecutionEntity.getTestSuiteExecution().getTestExecution().getId()+"";
				TestExecutionEntity testExecutionEntity = testExecutionRepository.findById(testCaseExecutionEntity.getTestSuiteExecution().getTestExecution().getId()).orElse(null);
				if(testExecutionEntity != null) {
					if(testExecutionEntity.getStopRequestDate() == null) {
						testExecutionEntity.setStopRequestDate(LocalDateTime.now());
						testExecutionRepository.flush();
					}
				}else {
					throw new GenericException(THE_REQUEST_DOESN_T_MATCH_THE_LOCAL_DATA_BASE_CONFIGURATION);
				}
				
			}
			
		}
		
		// --------------------------------------------
		// Send STOP request to ITB system
		// --------------------------------------------
		
		SessionStopResponse sessionStopResponse = itbTestSessionsManagementService.stopTestSession(sessionStopRequest);
		
		sessionStopResponse.setExecutionProgressive(executionProgressive);
		
		return sessionStopResponse;
	}
	
	/**
	 * 
	 * Get test cases to be executed
	 * 
	 * @param localSessionStartRequest
	 * @return
	 */
	private List<TestCaseEntity> getTestCasesConfigurationForStart(TestSessionStartRequest localSessionStartRequest) {
		
		List<TestCaseEntity> globalTestCaseEntityList = new ArrayList<>();

		if(localSessionStartRequest.getTestCase() != null && !localSessionStartRequest.getTestCase().isEmpty()) {

			// Test Cases List provided

			// Get all TC
			globalTestCaseEntityList = testCaseRepository.findByIdTestCaseIdIn(localSessionStartRequest.getTestCase());

			if (globalTestCaseEntityList != null && !globalTestCaseEntityList.isEmpty()
					&& localSessionStartRequest.getTestSuite() != null
					&& !localSessionStartRequest.getTestSuite().isEmpty()) {

				// If the TSs list is provided, filter on TC belonging to the given TSs
				ListIterator<TestCaseEntity> iter = globalTestCaseEntityList.listIterator();
				while (iter.hasNext()) {
					TestCaseEntity testCaseEntity = iter.next();
					boolean found = false;
					for (String testsuiteId : localSessionStartRequest.getTestSuite()) {
						if (testsuiteId.equals(testCaseEntity.getId().getTestSuiteId())) {
							found = true;
							break;
						}
					}
					if (!found) {
						iter.remove();
					}
				}
			}
		}else if (localSessionStartRequest.getTestSuite() != null && !localSessionStartRequest.getTestSuite().isEmpty()) {

			// Only Test Suites List provided

			// Get all TCs belonging the the given TSs
			List<TestCaseEntity> testCaseEntityList = testCaseRepository.findByTestSuiteIdIn(localSessionStartRequest.getTestSuite());
			globalTestCaseEntityList.addAll(testCaseEntityList);

		}
		
		return globalTestCaseEntityList;
	}
}
