FROM eclipse-temurin:21-jdk

CMD mkdir /app
CMD mkdir /app/config
COPY target/*.jar /app/poc-backend.jar
COPY simpl-open-poc-charts /app/simpl-open-poc-charts/
EXPOSE 8090
ENTRYPOINT ["java", "-jar", "/app/poc-backend.jar"]